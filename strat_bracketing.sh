cd output

# generate constraints

reps=2

# uniform priors

for i in fossils/tree_*
do
  dir="$i"
  dir+="_mu"
  dir=$(echo $dir | cut -f 2 -d '/')
  cp -r $i $dir
  cd $dir
  perl ../../Program.pl -f 5 -l f5 -t $reps -r 50 -u 2 -k M -j U
  cd ../
done

# non-uniform priors

for i in fossils/tree_*
do
  dir="$i"
  dir+="_mn"
  dir=$(echo $dir | cut -f 2 -d '/')
  cp -r $i $dir
  cd $dir
  perl ../../Program.pl -f 5 -l f5 -t $reps -r 50 -u 2 -k M -j N
  cd ../
done

mkdir strat_bracketing
mv *mn* strat_bracketing
mv *mu* strat_bracketing
