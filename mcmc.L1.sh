
cd output/phylo_bracketing
#cd output/strat_bracketing
#cd output/arbitrary_max

# mcmc analysis

reps=2

# posterior L = 1

seed=92

for i in tree_*
do
  cd $i
  for j in `seq 1 $reps`
  do
  	perl ../../../Program.pl -f 7 -l f7L1.$j -t 1 -u 2 -F $j -B $seed -L 1 -x 160000
  	seed=$(($seed + 1))
  done	
  cd ../
done
