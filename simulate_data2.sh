tree="trees/tree_u.txt"
reps=2
loci=20

mkdir output
mkdir output/tree_u_seqs

# parse trees

function Parse {

for i in `seq 1 $reps`
do
    mkdir output/tree_u_seqs/tree$i
    cp $tree output/tree_u_seqs/tree$i/tree$i.txt
done

}

Parse

# simulate sequences

cd output/tree_u_seqs

perl ../../Program.pl -f 4 -l f4 -t $reps -n 16 -L $loci -B 1234

cd ../

# simulate fossils

function SimOcc {

    targetDir=$1
    sampling=$2

    cp -r tree_u_seqs $targetDir
    cd $targetDir
    perl ../../Program.pl -f 2 -t $reps -r 50 -s $sampling -l f2 -B 1234
    cd ../

}

function SimOccNonUnif {

    targetDir=$1
    sampling=$2

    cp -r tree_u_seqs $targetDir
    cd $targetDir
    perl ../../Program.pl -f 3 -t $reps -r 50 -A $sampling -D 1 -T 1 -l f3 -B 1234
    cd ../

}

SimOcc "tree_u_unif_s_01" 1
SimOcc "tree_u_unif_s_02" 0.1
SimOcc "tree_u_unif_s_03" 0.01
SimOcc "tree_u_unif_s_04" 0.001

SimOccNonUnif "tree_u_non_unif_s_01" 1
SimOccNonUnif "tree_u_non_unif_s_02" 0.1
SimOccNonUnif "tree_u_non_unif_s_03" 0.01
SimOccNonUnif "tree_u_non_unif_s_04" 0.001

mkdir seqs fossils
mv tree_u_seqs seqs
mv tree_u* fossils


