
cd output/phylo_bracketing
#cd output/strat_bracketing
#cd output/arbitrary_max

# mcmc analysis

reps=2

# posterior L = 20

seed=1823

for i in tree_*
do
  cd $i
  for j in `seq 1 $reps`
  do
  	perl ../../../Program.pl -f 7 -l f7L20.$j -t 1 -u 2 -F $j -B $seed -L 20 -x 3200000
  	seed=$(($seed + 1))
  done	
  cd ../
done
