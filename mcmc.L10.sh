
cd output/phylo_bracketing
#cd output/strat_bracketing
#cd output/arbitrary_max

# mcmc analysis

reps=2

# posterior L = 10

seed=1689

for i in tree_*
do
  cd $i
  for j in `seq 1 $reps`
  do
  	perl ../../../Program.pl -f 7 -l f7L10.$j -t 1 -u 2 -F $j -B $seed -L 10 -x 1600000
  	seed=$(($seed + 1))
  done	
  cd ../
done
