## This program uses perl, R and unix commands and was developed using perl 5.10 and R 3.3. 

## Required R packages: sn and ape.

## To simulate sequence data or perform MCMC analysis using this program you need to download and compile the PAML programs (this requires having C installed)

# 1. Change to the directory simulation-study/Program/paml4.8/

cd Program/paml4.8

# 2. Run the following script to install paml and allow user specified file paths in mcmctree (required by Program/MCMC functions)

sh install_paml.sh

# 3. Return to the project directory simulation-study/

cd ../../

## To generate simulated datasets (fossils and sequences)

sh simulate-data1.sh # balanced tree
sh simulate-data2.sh # unbalanced tree

## To generate constraints

sh phylo_bracketing.sh
sh strat_bracketing.sh
sh arbitrary_max.sh

## MCMC analysis

sh mcmc.sh

## Models for simulating fossils are also available as part of the R package FossilSim

