
cd output

# generate constraints

reps=2

# uniform priors

for i in fossils/tree_*
do
  dir="$i"
  dir+="_bu"
  dir=$(echo $dir | cut -f 2 -d '/')
  cp -r $i $dir
  cd $dir
  perl ../../Program.pl -f 5 -l f5 -t $reps -r 50 -u 2 -k B -j U
  cd ../
done

# uniform priors

for i in fossils/tree_*
do
  dir="$i"
  dir+="_bn"
  dir=$(echo $dir | cut -f 2 -d '/')
  cp -r $i $dir
  cd $dir
  perl ../../Program.pl -f 5 -l f5 -t $reps -r 50 -u 2 -k B -j N
  cd ../
done

mkdir phylo_bracketing
mv *bn* phylo_bracketing
mv *bu* phylo_bracketing
