
cd output/phylo_bracketing
#cd output/strat_bracketing
#cd output/arbitrary_max

# mcmc analysis

reps=2

# prior L = 0

seed=6308

for i in tree_*
do
  cd $i
  for j in `seq 1 $reps`
  do
  	perl ../../../Program.pl -f 6 -l f6.$j -t 1 -u 2 -F $j -B $seed
  	seed=$(($seed + 1))
  done	
  cd ../
done
