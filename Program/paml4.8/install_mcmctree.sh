# download paml
curl -L "http://abacus.gene.ucl.ac.uk/software/paml4.8a.macosx.tgz" > paml4.8a.macosx.tgz

# unzip the file
tar -xvzf paml4.8a.macosx.tgz

# extract necessary files 
mv paml4.8/src/* .

# remove unnecessary files 
rm -r paml4.8

# edit mcmctree.c 

# line 249
sed -i '' -e 's/GenerateBlengthGH("out.BV")/GenerateBlengthGH("out.BV", dir)/' mcmctree.c
# line 981
sed -i '' -e 's/line, "baseml %s", ctlf/line, "%s\/baseml %s", dir, ctlf/' mcmctree.c
# line 47, 923
sed -i '' -e 's/GenerateBlengthGH (char infile\[\]/GenerateBlengthGH (char infile\[\], char dir\[\]/' mcmctree.c
# line 214 - 218
sed -i '' -e '/127/ a\
\ \ \ if(argc>2)\'$'\n \ \ \ \ \ \ strncpy(dir, argv[2], 127);
' mcmctree.c
# line 189
sed -i '' -e '/mcmctree.ctl/ a\
\ \ \ char dir\[512\]  = "/usr/bin";
' mcmctree.c

# compile paml programs
make evolver
make baseml
make mcmctree

