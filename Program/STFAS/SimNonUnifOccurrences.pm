#!/usr/bin/perl
use strict;
use warnings;
use ReadTree;

# This function simulates stratigraphic occurrence data under a non-uniform model of preservation (Holland 1995, 2000)
# Holland SM. 1995. The stratigraphic distribution of fossils. Paleobiology 21:92–109.
# Important points:
# 1) It is extremely important to note that this function simulates occurrence data along stem lineages and that these branches are labelled by all descendant terminals.
# This is contrast to most other functions in this package, which use an identical labelling scheme to refer to nodes
# 2) It is also important to note that horizon's are labeled using the maximum age of the horizon
# 3) The number of decimal places output is based on the resolution (e.g. total number number of horizons). See the final loop for details)
# Note the function can handle any given combination of maximum age + resolution (horizon number)

sub SimNonUnifOccurrences {

my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{y};
my $resolution=$options{r};
my $PD=$options{D};
my $DT=$options{T};
my $PA=$options{A};
my $max_age=$options{a};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
#ReadTree - subroutine written by Julian Gough
my @temp=&ReadTree($treefile,$verbosity); # subroutine returns (\%nodeup,\%distances,\%nodedown);

$i=$temp[0]; # first parameter is stored in $temp[0]
my %parent=%$i; # place %nodeup in hash %parent

$i=$temp[1]; # second parameter is stored in $temp[1]
my %distances=%$i; # place %distances in hash %distances

# ----
# open water depth profile
open (WATER, "../water.depth.$resolution.txt") || die "can't open water depth file\n";
my @lines = <WATER>;

my @water_depth;

foreach my $lines (@lines) {
		if ($lines =~ m/\d+/) {			
			chomp $lines;
			push @water_depth, "$lines";
		}
}
# check the water depth file has been input correctly
if ($verbosity==1) {
	print "water depth array @water_depth\n";
}
close WATER;

# ----
# Simulating occurrence data
my %done;
my %nodelabels;
my %occurrences;

my $horizon_length=$max_age/$resolution;
my $current_waterdepth;

# simulate occurrence data
foreach my $horizon (1..$resolution) { # being loop A.
	$current_waterdepth=$water_depth[$horizon-1];
	$horizon=($horizon/$resolution) * $max_age;
	
	# keep track of the horizon number/age/water depth
	if ($verbosity==1) {
		print "\nHorizon maximum is ", $horizon, "\n", "\nCurrent water depth is ", $current_waterdepth, "\n";
	}
		
	A: foreach my $node (sort(keys %parent)) { # begin loop 1
		# define 'node' as each key in the # hash 'parent'
		# in this loop, 'node' really means 'lineage'
	
		# keep track of the node/lineage here
		if ($verbosity==1) {
			print "\n", "Part 1 ", $node, "\n";
		}
			
		unless ($node =~ /:/) { # begin loop 2
		# skip if 'node' contains ':'
		# run through loop for terminals only
	
			# keep track of the node/lineage here
			if ($verbosity==1) {	
				print "Part 2 ", $node, "\n";
			}
				
			my $time=0; # set current time to zero for all terminal nodes
			my $fromtime=0;
			my $Rel_prob=0;
		
			while (exists($parent{$node})) { # being loop 3
				# while there exists a parent node
				# I'm not sure when this wouldn't be the case, check for larger-trees
						
				$time=$time+$distances{$node}; # define age of parent node
				
				# check if lineage is extant during current horizon
				if (($time >= ($horizon-$horizon_length) ) && ($fromtime <= $horizon) ) { # begin loop 4

					# is the script estimating the extant/extinct status of each lineage correctly?
					if ($verbosity==1) {
							print "Part 3 node age/time is $time \nPart 3 fromtime is $fromtime \nPart 3 lineage is EXTANT during horizon $horizon \n";
					}	

					# keep track of the relative Pr(preservation)
					if ($verbosity==1) { # this should be zero here
						print "Part 4 The relative probability of preservation (Rel prob) is $Rel_prob \n";
					}
					
					# calculate relative lineage duration
					if ($time <= $horizon) {
						
						if ($verbosity==1) {
							print "Part 4 lineage $node became extinct before the end of horizon $horizon at time $time\n";
						}
						
						$Rel_prob=($horizon_length-($horizon-$time))/$horizon_length;
					}
					elsif (($fromtime >= ($horizon-$horizon_length) ) && ($fromtime <= $horizon) ) {
						
						if ($verbosity==1) {
							print "Part 4 lineage $node became extant before the beginning of horizon $horizon at time $fromtime\n";
						}

						$Rel_prob=1-(($horizon_length-($horizon-$fromtime))/$horizon_length);
					}
					else {
						
						if ($verbosity==1) {
							print "Part 4 lineage $node is extant for the entire duration of horizon $horizon\n";
						}
						
						$Rel_prob=1;
					}

					if ($verbosity==1) {
						print "Part 4 Rel_prob is $Rel_prob for $node during horizon $horizon\n";
					}
										
					# generate random number between 0 and 1
					my $range = 1;
					
					my $random_number = rand($range);
					
					# keep track of simulated fossil occurrences
					if ($verbosity==1) {
						print "Part 5 random_number is $random_number \n";
					}
										
					# calculate the probability of collection
					my $p_collection = $PA * exp( (-($current_waterdepth-$PD)**2) / (2 * ($DT ** 2)) );
					
					if ($verbosity==1) {
						print "p(collection) = $p_collection\n";
					}
					
					# generate occurrence data
					if ($random_number <= $p_collection*$Rel_prob) { # begin loop 5
						
						if ($verbosity==1) {
							print "Part 5 for $node during $horizon, fossil is collected \n";
						}
						
						# fossil is collected						
						push @{$occurrences{$node}}, $horizon;
						
					} # end loop 5
					
					else { # begin loop 6
						
						# fossil is not collected
						if ($verbosity==1) {
							print "Part 5 for $node during $horizon, fossil is not collected \n";
						}						

					} # end loop 6
								
				} # end loop 4
				
				else { # begin loop 7 
					
					# lineage is extinct
					if ($verbosity==1) {
						print "Part 3b node age/time is $time \nPart 3b fromtime is $fromtime \nPart 3b species is EXTINCT during horizon $horizon \n";
					}
				
				} # end loop 7
									
				$node=$parent{$node};
				# now refer to the parent node

				if ($verbosity==1) {
					print "Part 6a the parent node is ", $node, "\n";
				}
							
				unless (exists ($done{$node})) { # begin loop 8
					# unless this node has already been done
					
					if ($verbosity==1) {
						print "Part 6b node ", $node, " hasn't been dealt with\n";
					}					
				
					$nodelabels{$node}="$fromtime-$time";
					# create a nodelabel for this node
					
					if ($verbosity==1) {
						print "Part 6c the nodelabel for $node is ", $nodelabels{$node}, "\n";
					}					
				
					$fromtime=$time;
					# fromtime is now equal to the age at the current (parent) node

					if ($verbosity==1) {
						print "Part 7 fromtime is now ", $fromtime, "\n";
					}
				
					$done{$node}=1;
					# record node as done

					if ($verbosity==1) {
						print "Part 8 ", $node, " is recorded as done\n";
					}					
					
				} # end loop 8

				else { # begin loop 9
					
					next A;
					
				} # end loop 9

			} # end loop 3
			
		} # end loop 2
	
	} # end loop 1
		
	for (keys %done) { # begin loop B.
		delete $done{$_};
		# empty hash %done for each horizon
		
	} # end loop B.

} # end loop A.


# print occurrence data

open (OCC, ">Occurrence_data.txt") || die "can't open file";

if ($verbosity==1) {
	print "\n \%occurrences \n";
}

# work out how decimal places to record
# this is a somewhat arbitrary system
# but it is designed such that successive horizons should never be output with the same age as a consequence of rounding
# the number of decimal places is simply equal to the numbers of digits in the specified resolution
# e.g
# < 10 : 1 decimal
# > 10 < 100 : 2 decimals
# > 100 < 1000 : 3 decimals etc.
my $sig_figs=length($resolution);
# create a string for specifying the number of decimal places to be output
my $sprintf="%.$sig_figs"."f";

foreach my $lineage (sort(keys %occurrences)) {

	# to examine the structure of the occurrence hash
	if ($verbosity==1) {		
		print "$lineage: @{$occurrences{$lineage}}\n";
	}
	
	print OCC "$lineage:";
	
	# print out all occurrence data
	foreach my $occurrence (@{$occurrences{$lineage}}) {
		
		# round horizon ID (equivalent to the maximum age) to the nearest decimal place
		# the number of decimal places is specified above
		my $output=sprintf("$sprintf", $occurrence);
		print OCC " $output";
		
		if ($verbosity==1) {
			print "occurrence: $occurrence\n";
			print "output: $output\n";
		}
		
	}
	print OCC "\n";
}

close OCC;

}

1;