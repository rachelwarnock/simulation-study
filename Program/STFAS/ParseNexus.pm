#!/usr/bin/perl
use strict;
use warnings;
use ReadTree;

# This script parses a nexus format tree file
# and creates an individual treefile and folder for each tree
# the function has the ability to warn the user if the number of taxa has been specified incorrectly

sub ParseNexus {
	
my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{i};
my $trees=$options{t};
my $taxa;
if ($options{n}) {
	$taxa=$options{n};
}

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----

# open the treefile		
open (TREES, "$treefile") || die "can't open treefile. Specify using -i\n";
	
my @lines = <TREES>;

# ----
	
my $tree_number=0;

# count the number of trees in the nexus file
foreach my $lines (@lines) {
	if ($lines =~ m/^.*=\s?\[?&?[RU]?\]?\s?(\(.+;)$/x ) {
		$tree_number=$tree_number+1;
	}
}

# kill the program if the number of trees specifed by the user exceeds the number in the file
if ($tree_number < $trees) {
	die "there aren't enough trees in the nexus file!\n";
}

# ----

my $tree_counter=1;
my $current_tree;

foreach my $lines (@lines) {
	
	unless ($tree_counter > $trees) {
		if ($lines =~ m/^.* # this regex recognises lines containing trees TREE * UNTITLED = [&R] (t3:1, ...
					=\s?
					\[?
					&?
					[RU]?
					\]?
					\s?
					(\(.+;)
					$/x ) {

			$current_tree="tree$tree_counter.txt";
				
			if ($verbosity==1) {
				print "Current tree = $current_tree\n";
			}
				
			open (TREE, ">$current_tree") || die "can't open file\n";
			print TREE $1, "\n";
			close TREE;
			
			# ----
			# check the number of taxa matches the number in the trees
			if ($taxa) {
				if ($tree_counter == 1) {
					checkTaxonCount($current_tree,$taxa,$verbosity);
				}
			}						
			# ----
			
			system ("mkdir tree$tree_counter");
			system ("mv $current_tree tree$tree_counter");
			$tree_counter=$tree_counter+1;
		}
	}
}

close TREES;
	
}

sub checkTaxonCount {

my $current_tree=$_[0];
my $taxa=$_[1];
my $verbosity=$_[2];			
			
#ReadTree - subroutine written by Julian Gough
my @temp=ReadTree($current_tree,$verbosity); # subroutine returns (\%nodeup,\%distances,\%nodedown);

my $i=$temp[0]; # first parameter is stored in $temp[0]
my %parent=%$i; # place %nodeup in hash %parent
				
# calculate the number of terminals & the number of nodes
# required to define the maximum number of times loop 5 looks for a parent node
my $terminals=0;
foreach my $terminal (keys %parent) {
unless ($terminal =~/:/) {
	$terminals=$terminals+1;
	}					
}

if ($terminals != $taxa) {
	die "the specified number of taxa $taxa (-n) doesn't match the number of taxa in the tree\n";
}	
	
}

1;
