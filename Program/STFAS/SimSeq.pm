#!/usr/bin/perl
use strict;
use warnings;

# This function simulates sequence data under a strict or relaxed clock model
# The function requires the R package ape to simulate sequences under the relaxed clock model

sub SimSeq {

my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{y};
my $taxa=$options{n};
my $variance=$options{z};
my $characters=$options{C};
my $seq_replicates=$options{S};
my $loci=$options{L};
my $model=$options{M};
my $program_dir=$options{P};
my $overall_mean=$options{W};
#my $overall_mean=1;

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
# create a label for all output files
my ($current_tree_abbrev, $txt) = split(/\./, $treefile);

# ----
# check the number of taxa is correct
# read tree
my @temp=&ReadTree($treefile,$verbosity);
$i=$temp[0]; # first parameter is stored in $temp[0]
my %parent=%$i; # place %nodeup in hash %parent

# calculate the number of terminals & the number of nodes
# required to define the maximum number of times loop 5 looks for a parent node
my $terminals=0;
foreach my $terminal (keys %parent) {
	unless ($terminal =~/:/) {
		$terminals=$terminals+1;
	}					
}
if ($terminals != $taxa) {
	die "the specified number of taxa $taxa (-n) doesn't match the number of taxa in the tree\n";
}

# ----
# calculate the standard deviation # I don't think this is necessary
my $stdev=($variance**(1/2));
$stdev=sprintf("%.5f",$stdev);

# ----
# generate a random seed for R
my $Rseed = int(rand(2**31));

# ----
# for relaxed clock sequence data - generate a tree with rate variable branches
if ($variance > 0) {
	
	my $internal_branches=($taxa*2)-2;

	open (R_COMS, ">Rate_hetero.R") || die "can't open file\n";
	my $commands_for_R = "# load TreeSim and required packages (includes ape and geiger)
	library(ape)

	set.seed($Rseed)

	# read true tree
	$current_tree_abbrev<-read.tree(\"$current_tree_abbrev.txt\")
	
	loci=$loci
	#rate.log <- file(\"mean.rates.log\") - what is this?
	
	sim.hetero<-function(loci)
	
	{
		
	loci=loci
	
	for (i in 1:loci) {
		
		# generate a mean substitution rate for each locus
		# gamma distribution
		#mean.rate=rgamma(1,2,(2/$overall_mean))
		mean.rate=rgamma(1,shape=2,scale=1/2)
		
		# normal distribution
#		mean.rate=rnorm(1,$overall_mean,0.1)
#		if (mean.rate < 0) {
#			mean.rate=mean.rate*-1
#		}
		
		#fixed mean rate
		#mean.rate=$overall_mean
		
		# log the rates
		sink(\"mean.rates.log\", append=TRUE)
		print(mean.rate)
		sink()
		
		# simulate variable rates under a log normal distribution
		#simulated.rates=exp(rnorm($internal_branches,mean.rate,$stdev))
		#simulated.rates=rlnorm($internal_branches,((log(mean.rate)-$variance)/2),($variance*(1/2))) # see dos Reis et al., 2014
		simulated.rates=rlnorm($internal_branches,log(mean.rate),$stdev)		
		
		# generate new branch lengths
		new.blens=$current_tree_abbrev\$edge.length*simulated.rates
		
		# view new branch lengths
		#print(new.blens)
		new.tree<-$current_tree_abbrev
		
		# generate a tree with the new branch lengths
		new.tree\$edge.length = new.blens
		
		# print the new tree
		loci.id=i-1
		filename=paste(\"$current_tree_abbrev.new.blens.v$variance.L\",loci.id,\".txt\",sep=\"\")
		write.tree(new.tree, file=filename)
	}
		
	}
	
	sim.hetero(loci)";
	
	print R_COMS $commands_for_R;

	if ($verbosity==1) {
		#system ("R --vanilla --no-save < Rate_hetero.R");
		system ("R --vanilla --slave --no-save < Rate_hetero.R");
	}
	else {		
		system ("R --vanilla --slave --no-save < Rate_hetero.R");
	}
}

# ----
my $tree;
# for relaxed clock sequence data
# create a string containing the tree generated using R (ape)
my $counter;
my $loci_counter=0;

A: if ($variance > 0) {
	open (TREE, "$current_tree_abbrev.new.blens.v$variance.L$loci_counter.txt")|| die "can't open file";

	my @lines = <TREE>;

	foreach my $lines(@lines) {
		if ($lines =~ m/^\(/ ) {
			$tree = $lines;
		}
	}

	close TREE;
}
# otherwise create a string containing the tree with true ages
else {
	print $treefile, "\n";
	open (TREE, "$treefile") || die "can't open file";

	my @lines = <TREE>;
	
	foreach my $lines(@lines) {
		if ($lines =~ m/^\(/ ) {
			$tree = $lines;
		}
	}

	close TREE;
}

# ----
# generate random seed for evolver
my $paml_seed = int(rand(2**31));

# ----
# generate the input file for evolver
open (SEQ_INPUT, ">$current_tree_abbrev.MCbase.dat") || die "can't open file";

my $commands_for_paml = "0          * 0: paml format (mc.paml)
$paml_seed   * random number seed (odd number)

$taxa $characters $seq_replicates  * <# seqs>  <# nucleotide sites>  <# replicates>
-1         * <tree length, use -1 if tree below has absolute branch lengths>

$tree

$model          * model: 0:JC69, 1:K80, 2:F81, 3:F84, 4:HKY85, 5:T92, 6:TN93, 7:REV
5 * kappa or rate parameters in model
0.2500  5     * <alpha>  <#categories for discrete gamma>

0.25  0.25  0.25  0.25    * base frequencies default values? 0.25318  0.32894  0.31196  0.10592
  T        C        A        G\n";

print SEQ_INPUT $commands_for_paml;
close SEQ_INPUT;

# ----
# specify the file path for evolver
my $evolver=$program_dir."Program/paml4.?/evolver";
# run evolver
system ("$evolver 5 $current_tree_abbrev.MCbase.dat");

# ----
# For multiple sequence replicates create individual alignment files
open (REPS, "mc.paml") || die "can't open file\n";

my @reps = <REPS>;
my $rep_string='';

# put all the alignments in a single array
foreach my $reps(@reps){
	$rep_string=$rep_string.$reps;
}

# split into individual alignments
my @rep_split = split(/\n\n\n\n/, $rep_string);

$counter=0;

# print each alignment
foreach my $rep_split(@rep_split) {
	open (SEQ_REP, ">$current_tree_abbrev.L$loci_counter.$counter.seq");
	print SEQ_REP $rep_split, "\n\n";
	close SEQ_REP;
	$counter=$counter+1;
}
undef @reps;
undef @rep_split;
close REPS;

$loci_counter=$loci_counter+1;

unless ($loci_counter == $loci) {
	goto A;
}

# -----
# concatenate loci replicates 
foreach my $seq_rep(0..$seq_replicates-1) { 
	system("cat *.$seq_rep.seq > $current_tree_abbrev.$seq_rep.seq");
	unless ($verbosity == 1) {
		system("rm $current_tree_abbrev.L*.$seq_rep.seq");
	}
}

# -----
# clean up
unless ($verbosity == 1) {
	system ("rm ancestral.txt mc.paml evolver.out siterates.txt $current_tree_abbrev.MCbase.dat");
	if ($variance > 0) {
		system ("rm $current_tree_abbrev.new.blens* Rate_hetero.R");
	}	
}

system("mv mean.rates.log mean.rates.L$loci.log")


# -----

}

1;
