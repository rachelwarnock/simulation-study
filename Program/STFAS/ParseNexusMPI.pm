#!/usr/bin/perl
use strict;
use warnings;

# This script parses a nexus format tree file into subfiles for use when the program is run in parallel

sub ParseNexusMPI {
	
my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{i};
my $trees=$options{t};

# ?? number of processors?
my $processor_max=4;

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# -----

# open the treefile		
open (TREES, "$treefile") || die "can't open treefile. Specify using -i\n";
	
my @lines = <TREES>;

# -----
	
my $tree_number=0;

# count the number of trees in the nexus file
foreach my $lines (@lines) {
	if ($lines =~ m/^.*=\s?\[?&?[RU]?\]?\s?(\(.+;)$/x ) {
		$tree_number=$tree_number+1;
	}
}

# kill the program if the number of trees specifed by the user exceeds the number in the file
if ($tree_number < $trees) {
	die "there aren't enough trees in the nexus file!\n";
}

# -----

my $tree_counter=0;
my $tree_start;
my $tree_stop;
my $trees_printed=0;

foreach my $process (1..$processor_max) { # begin loop 1

	print "$process\n";

	open (TREE, ">simulated_trees.$process.nex") || die "can't open file\n";

	$tree_start=($process*10)-10;
	
	print "$tree_start\n";
	
	$tree_stop=$tree_start+10;
	
	
	print "$tree_stop\n";

	# for each line in the nexus file
	foreach my $lines (@lines) { # begin loop 2
		
		# unless line contains a tree		
		unless ($lines =~ m/^.*=\s?\[?&?[RU]?\]?\s?(\(.+;)$/x ) { # begin loop 3
		# this regex recognises lines containing trees TREE * UNTITLED = [&R] (t3:1, ...
		
			print TREE $lines;				
				
		} # 3
		
		# if line contains a tree
		if ($lines =~ m/^.*=\s?\[?&?[RU]?\]?\s?(\(.+;)$/x ) { # begin loop 4
			
			# if we've reached the start tree to print
			if ($tree_counter >= $tree_start) { # begin loop 5
				
				$trees_printed=$trees_printed+1;

				# unless we've printed the enough trees
				unless ($tree_counter >= $tree_stop) { # begin loop 6
				
					print TREE $lines;
				} # 6							
			} # end loop 5
			
			# count the number of trees
			$tree_counter=$tree_counter+1;
			
		} # end loop 4
		
	} # end loop 2
	
	# reset the tree counters for the next processor
	$tree_counter=0;
	$trees_printed=0;
	
} # end loop 1
	
close TREES;
	
}

1;
