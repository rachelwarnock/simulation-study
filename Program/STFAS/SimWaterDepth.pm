#!/usr/bin/perl
use strict;
use warnings;
use Math::Trig;

# this function simulates a water depth profile using a simple sine wave function
# see below R commands

sub SimWaterDepth {

my $i=$_[0];
my %options=%$i;

# required input
my $resolution=$options{r};
my $max_depth=$options{w};

# number of trangression/regression events to cycle through across the time interval
my $cycles=2;

# calculate the number of horizons after t = 0
my $total_minus_zero=$resolution-1;
# calculate the value with which to evenly increase the x-axis values
my $multiples=$cycles/$total_minus_zero;

# define the x-axis value at t = -1
my $x=-$multiples;
my $y; # water depth at t = x
my @y_values; # this array contains the water depth at during each  

foreach my $current_depth (0..$total_minus_zero) {
	
	# y = a * sin (b * pi * (x-1/c))
	# # a - total depth excursion
	# # b - number of cycles
	# # 1/c - defines the relative start time of each cycle
		
	$x=$x+$multiples;
	my $y=$max_depth*sin($cycles*pi*($x-1/4));
	push(@y_values,$y);	
	
}

open (DEPTH, ">water.depth.$resolution.txt");

foreach $y (@y_values) {
	print DEPTH "$y\n";
}

close DEPTH;
	
}

1;

# the equivalent commands will do the same in R
# # define the x-axis values
# x=seq(0,$cycles,length.out=$resolution)
# define y-axis values
# # y = a * sin (b * pi * (x-1/c))
# # a - total depth excursion
# # b - number of cycles
# # 1/c - defines the relative start time of each cycle
# y=$depth*sin($cycles*pi*(x-1/4))
# # if you want to see a plot of the water depth profile
# plot(x,y, type="l")
# # export a table with changing water depth over time
# write.table(y, file="water.depth.txt", quote=FALSE)


