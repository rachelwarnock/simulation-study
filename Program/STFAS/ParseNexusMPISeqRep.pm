#!/usr/bin/perl
use strict;
use warnings;

# this function parses the treefile simulated_trees.nex for large scale MPI runs
# a nexus file is created for each tree, and printed to the file as many times as there are specified sequence replicates 

# note this function doesn't check if the number of taxa in each tree is correct

sub ParseNexusMPISeqRep {
	
my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{i};
my $trees=$options{t};
my $taxa=$options{n};
my $reps=$options{S};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# -----

# open the treefile		
open (TREES, "$treefile") || die "can't open treefile. Specify using -i\n";
	
my @lines = <TREES>;

# -----
	
my $tree_number=0;

# count the number of trees in the nexus file
foreach my $lines (@lines) {
	if ($lines =~ m/^.*=\s?\[?&?[RU]?\]?\s?(\(.+;)$/x ) {
		$tree_number=$tree_number+1;
	}
}

# kill the program if the number of trees specifed by the user exceeds the number in the file
if ($tree_number < $trees) {
	die "there aren't enough trees in the nexus file!\n";
}

# -----

my $tree_counter=1;
my $current_tree;

foreach my $lines (@lines) {
	
	if ($tree_counter <= $trees) {

		if ($lines =~ m/^.* # this regex recognises lines containing trees TREE * UNTITLED = [&R] (t3:1, ...
					=\s?
					\[?
					&?
					[RU]?
					\]?
					\s?
					(\(.+;)
					$/x ) {
						
			my $tree = $1;
			
		
#			my $rank = $tree_counter-1;

#			$current_tree="simulated_tree.$rank.nex";
			$current_tree="simulated_tree.$tree_counter.nex";
				
			if ($verbosity==1) {
				print "Current tree = $current_tree\n";
			}				
			
			# ----
						
			open (NEXUS, ">$current_tree") || die "can't open file\n";
			print NEXUS "#NEXUS\n";
			print NEXUS "BEGIN TAXA;\n";
			print NEXUS "\tDIMENSIONS NTAX = $taxa;\n";
			print NEXUS "\tTAXLABELS\n";
			
			foreach my $label (1 .. $taxa) {
				print NEXUS "\t\tt$label\n";
			}
			
			print NEXUS "\t;\nEND;\n";
			print NEXUS "BEGIN TREES;\n";
			
			foreach my $reps (1 .. $reps) {
				print NEXUS "\tTREE * U UNTITLED = [&R] ";
				print NEXUS $tree, "\n";
			}
			
			print NEXUS "END;\n";
			
			close NEXUS;
			
			# -----
			
#			system ("mkdir Simulated_tree_$rank");
#			system ("mv $current_tree simulated_tree_$rank");
			system ("mkdir Simulated_tree_$tree_counter");
			system ("mv $current_tree Simulated_tree_$tree_counter");
			$tree_counter=$tree_counter+1;
		}
	}
}

close TREES;
	
}

1;
