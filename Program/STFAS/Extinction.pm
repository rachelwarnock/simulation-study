#!/usr/bin/perl
use strict;
use warnings;

# this function is choses the extinction rate, relative to 
# a fixed speciation rate = 1 (for n < 150) or 1 (for n > 150) 
# this function isn't actually integrated into the analysis, but can be called used TestScripts/Extinction.pl

# see Pennell et al., 2012

sub Extinction {

my $i=$_[0];
my %options=%$i;

# required options
#my $terminals=$options{n};
my $age=$options{a};
my $speciation=$options{g};
my $extinction=$options{e};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
# calculate the number of expected tips
my $rate;
my $expected_tips;

$rate=$speciation-$extinction;

$expected_tips=2*exp($rate*$age);

$expected_tips=sprintf("%.f", $expected_tips);

#if ($verbosity == 1) {
	print "speciation rate $speciation\n";
	print "extinction rate $extinction\n";
	print "turnover rate $rate\n";
	print "expected number of tips $expected_tips\n";
#}

# ----
#return($speciation,$extinction);

}


1;

