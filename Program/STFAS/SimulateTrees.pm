#!/usr/bin/perl
use strict;
use warnings;

# This function simulates trees using Tanja Stadler's TreeSim package
# http://cran.r-project.org/web/packages/TreeSim/index.html
# written & tested using R v. 2.15

sub SimulateTrees {
	
my $i=$_[0];
my %options=%$i;

# required input
my $taxa=$options{n}; # number of taxa
my $trees=$options{t}; # number of trees
my $birth=$options{g}; # origination/speciation rate (lamda)
my $death=$options{e}; # extinction rate (mu)
my $age=$options{a}; # root age
my $simualted_trees_file=$options{i};
my $sampling_fraction=1;


# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# create R commands file
open (TREESIM_COMS, ">TreeSim_commands.R") || die "can't create TreeSim commands file!\n";

my $commands = "# load TreeSim and required packages (includes ape and geiger)
library(TreeSim)

# simulate birth death trees, specify paras: no. of extant leaves, tree number, lamda (speciation), mu (extinction), sampling fraction & age
simulated_trees<-sim.bd.taxa.age($taxa, $trees, $birth, $death, $sampling_fraction, $age, mrca=TRUE)

# save simulated BD trees to a nexus file
# n.b this command sometimes causes R to crash & report a segmentation fault
write.nexus(simulated_trees, file=\"$simualted_trees_file\", translate=FALSE)";

print TREESIM_COMS $commands;

close TREESIM_COMS;

if ($verbosity==1) {
	system ("R --vanilla --no-save < TreeSim_commands.R");
}
else {
	system ("R --vanilla --slave --no-save < TreeSim_commands.R");
}

unless ($verbosity==1) {
	system("rm TreeSim_commands.R");
}
	
}

1;
