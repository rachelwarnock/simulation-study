#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# SimWaterDepth.pl
# test function: SimWaterDepth
# package: STFAS
# other functions required: none

my %options=();

getopts("dr:w:v", \%options);

# function options
if (!$options{r}) {
	die "Specify the resolution using -r!\n";
}
if (!$options{w}) {
	die "Specify the maximum water depth using -w!\n";
}

# use $options{v} or $options{d} to switch on verbose mode

SimWaterDepth(\%options);

# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$stfas=$_;
}

BEGIN {

	use lib $stfas;
	use SimWaterDepth;

}
