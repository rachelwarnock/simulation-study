#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# SkewPr.pl
# test function: Hedman
# package: STFAS ConfidenceIntervals::Hedman::
# other functions required: ReadTree, ReadOccurrences, FirstAppearances
# OutgroupFADs, HedmanConsistent | HedmanConservative, ParseHedmanRoutput

my %options=();

getopts("a:dr:u:vy:", \%options);

# function options
if (!$options{y}) {
	die "Specify the treefile using -y!\n";
}
if (!$options{r}) {
	die "Specify the resolution using -r!\n";
}
if (!$options{a}) {
	die "Specify the maximum age of the basin using -a!\n";
}
if (!$options{u}) {
	die "Specify the upper bound using -u!\n";
}

# use $options{v} or $options{d} to switch on verbose mode

# ----
# locate source
my $program_dir;

my @dirs=split(/\//,$_);
pop(@dirs);
pop(@dirs);

$program_dir=join('/', @dirs);
$options{P}=$program_dir.'/';

# ----
# chose one of the following to generate min and max bounds:
# MinimaOnly | BestPractice | Marshall | Hedman
my $i;

my @temp=BestPractice(\%options);

$i=$temp[0];
my %minimum_bounds=%$i;
$i=$temp[1];
my %maximum_bounds=%$i;

# ----
# print tree for R

PrintTreeR(\%options,\%minimum_bounds,\%maximum_bounds);

# ----
# print tree for MCMCTREE

SkewPr(\%options);

# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$stfas=$_;
}

BEGIN {

	use lib $stfas;
	use ConfidenceIntervals::MinimaOnly;
	use ConfidenceIntervals::BestPractice;
	use ConfidenceIntervals::Marshall;
	use ConfidenceIntervals::Hedman::Hedman;
	use ConfidenceIntervals::PrintTreeR;
	use ConfidenceIntervals::SkewPr::SkewPr;
}

BEGIN {
	$program_dir=$current_dir;
}

