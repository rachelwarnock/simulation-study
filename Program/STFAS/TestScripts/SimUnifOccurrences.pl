#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# SimUnifOccurrences.pl
# test function: SimUnifOccurrences
# package: STFAS
# other functions required: ReadTree

my %options=();

getopts("a:dr:s:y:v", \%options);

# function options
if (!$options{y}) {
	die "Specify the treefile using -y!\n";
}
if (!$options{r}) {
	die "Specify the resolution using -r!\n";
}
if (!$options{s}) {
	die "Specify the sampling intensity using -s!\n";
}
if (!$options{a}) {
	die "Specify the maximum age in the tree using -a!\n";
}

# use $options{v} or $options{d} to switch on verbose mode

SimUnifOccurrences(\%options);

# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$stfas=$_;
}

BEGIN {

	use lib $stfas;
	use SimUnifOccurrences;

}
