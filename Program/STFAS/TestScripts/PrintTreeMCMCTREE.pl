#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# PrintTreeMCMCTREE.pl
# test functions: PrintTreeMCMCTREE
# package: STFAS ConfidenceIntervals::
# other functions required: ReadTree, ReadOccurrences, FirstAppearances & MinimaOnly | BestPractice | Marshall | Hedman
# and PrintTree

my %options=();

getopts("a:b:c:dm:p:r:u:vy:", \%options);

# function options (PrintCalibrations)
if (!$options{y}) {
	die "Specify the treefile using -y!\n";
}
if (!$options{u}) {
	die "Specify the upper bound using -u!\n";
}
if (!$options{p}) {
	die "Specify the p-value using -p!\n";
}
if (!$options{c}) {
	die "Specify the c-value using -c!\n";
}
if (!$options{m}) {
	die "Specify the minmum bound tail using -m!\n";
}
if (!$options{b}) {
	die "Specify the maximum bound tail using -b!\n";
}

# additional function options
if (!$options{r}) {
	die "Specify the resolution using -r!\n";
}
if (!$options{a}) {
	die "Specify the maximum age of the basin using -a!\n";
}

# use $options{v} or $options{d} to switch on verbose mode

# ----
# locate source (necessary for Hedman, SkewPr)
my $program_dir;

my @dirs=split(/\//,$_);
pop(@dirs);
pop(@dirs);

$program_dir=join('/', @dirs);
$options{P}=$program_dir.'/';

# ----
# chose one of the following to generate min and max bounds:
# MinimaOnly | BestPractice | Marshall | Hedman
my $i;

my @temp=Hedman(\%options);

$i=$temp[0];
my %minimum_bounds=%$i;
$i=$temp[1];
my %maximum_bounds=%$i;

# ----
# print tree for MCMCTREE

PrintTreeMCMCTREE(\%options,\%minimum_bounds,\%maximum_bounds);

# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$stfas=$_;
}

BEGIN {

	use lib $stfas;
	use ConfidenceIntervals::MinimaOnly;
	use ConfidenceIntervals::BestPractice;
	use ConfidenceIntervals::Marshall;
	use ConfidenceIntervals::Hedman::Hedman;
	use ConfidenceIntervals::PrintTreeMCMCTREE;

}
