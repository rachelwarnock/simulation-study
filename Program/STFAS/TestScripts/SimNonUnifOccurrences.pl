#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# SimNonUnifOccurrences.pl
# test function: SimNonUnifOccurrences
# package: STFAS
# other functions required: ReadTree, SimWaterDepth

my %options=();

getopts("A:a:D:dr:T:y:v", \%options);

# function options
if (!$options{y}) {
	die "Specify the treefile using -y!\n";
}
if (!$options{r}) {
	die "Specify the resolution using -r!\n";
}
if (!$options{D}) { # preferred depth
	die "Specify the preferred depth using -D!\n";
}
if (!$options{T}) { # depth tolerance
	die "Specify the depth tolerance using -T!\n";
}
if (!$options{A}) { # peak abundance
	die "Specify the peak abundance using -A!\n";
}
if (!$options{a}) { # maximum age
	die "Specify the maximum age using -a!\n";
}

# use $options{v} or $options{d} to switch on verbose mode

SimNonUnifOccurrences(\%options);

# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$stfas=$_;
}

BEGIN {

	use lib $stfas;
	use SimNonUnifOccurrences;

}
