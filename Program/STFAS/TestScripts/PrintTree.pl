#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# PrintTree.pl
# test function: PrintTree (subsroutine written by Julian Gough, University of Bristol)
# package: STFAS ConfidenceIntervals::
# other functions required: ReadTree

my %options=();

getopts("dy:v", \%options);

# function options
if (!$options{y}) {
	die "Specify the treefile using -y!\n";
}

my $treefile=$options{y};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
# read tree
my $i;
my @temp=ReadTree($treefile,$verbosity);

$i=$temp[0];
my %parents=%$i;
$i=$temp[2];
my %childs=%$i;
my %distances=(); # the hash distances returned by the function ReadTree contains the distance from each terminal/node to root

# ----
# print tree

my $newick=PrintTree(\%parents,\%distances,\%childs);

print "$newick\n";

# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$stfas=$_;
}

BEGIN {

	use lib $stfas;
	use ReadTree;
	use ConfidenceIntervals::PrintTree;

}
