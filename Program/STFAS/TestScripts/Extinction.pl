#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# Extinction.pl
# test function: Extinction
# package: STFAS
# other functions requied: none

my %options=();

getopts("a:de:g:n:v", \%options);

# function options
#if (!$options{n}) {
#	die "Specify the number of taxa using -n!\n";
#}
if (!$options{a}) {
	die "Specify the age of the root using -a!\n";
}
if (!$options{g}) {
	die "Specify the speciation rate using -g!\n";
}
if (!$options{e}) {
	die "Specify the extinction rate using -e!\n";
}

# use $options{v} or $options{d} to switch on verbose mode

Extinction(\%options);

# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$stfas=$_;
}

BEGIN {

	use lib $stfas;
	use Extinction;

}
