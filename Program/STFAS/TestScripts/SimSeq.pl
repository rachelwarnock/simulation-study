#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# SimSeq.pl
# test function: SimSeq
# package: STFAS
# other functions required: none
# requires R (ape libray)
# requires evolver (paml package)

my %options=();

getopts("C:dn:M:y:S:vz:", \%options);

# function options
if (!$options{y}) {
	die "Specify the treefile using -y!\n";
}
if (!$options{n}) {
	die "Specify the number of taxa using -n!\n";
}
if (!$options{C}) {
	die "Specify the alignment length -C!\n";
}
if (!$options{S}) {
	die "Specify the number of replicates using -S!\n";
}
if (!$options{M}) {
	die "Specify the substitution model using -M! (Default = 4)\n";
}

$options{z}=0.1 unless defined $options{z};

# use $options{v} or $options{d} to switch on verbose mode

# ----
my $program_dir; # Program directory
$options{P}=$program_dir;

# ----
SimSeq(\%options);

# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library


BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$stfas=$_;

}

BEGIN {

	use lib $stfas;
	use SimSeq;

}

BEGIN {
	
	# program directory	
	$program_dir=$stfas;
	$program_dir =~ s/Program\/STFAS\///g;
	
}
