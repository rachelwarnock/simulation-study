#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# Constraints.pl
# test function: Constraints
# package 
# other functions required: ReadTree, ReadOccurrences, FirstAppearances & MinimaOnly | BestPractice | Marshall | Hedman::Hedman (and associated functions)
# PrintTree, PrintTreeR, BeastUnif, LogNormalPr, PrintCalibrations, SkewPr::SkewPr (and associated functions)

my %options=();

getopts("a:b:Bc:dj:k:m:n:p:r:s:u:vy:", \%options);

# function options
if (!$options{y}) {
	die "Specify the treefile using -y!\n";
}
if (!$options{u}) {
	die "Specify the upper bound using -u!\n";
}
if (!$options{k}) {
	die "Specify the constraint type using -k!\n";
}
if (!$options{j}) {
	die "Specify uniform or non-uniform priors using -j!\n";
}

# $options{B} to use BEAST

# additional function requirements
if (!$options{r}) {
	die "Specify the resolution using -r!\n";
}
if (!$options{a}) {
	die "Specify the maximum age of the basin using -a!\n";
}

# optional function requirments
# "Specify the p-value using -p!\n"
$options{p}="0.1" unless defined $options{p};
# "Specify the c-value using -c!\n"
$options{c}="0.5" unless defined $options{c};
# "Specify the minmum bound tail using -m!\n"
$options{m}='1e-300' unless defined $options{m};
# "Specify the maximum bound tail using -b!\n"
$options{b}='1e-300' unless defined $options{b};

if ($options{k} eq 'S') {
	if (!$options{n}) {
		die "Specify extant diversity using -n!\n";
	}
	if (!$options{s}) {
		die "Specify preservation rate using -s!\n";
	}
}

# use $options{v} or $options{d} to switch on verbose mode

# ----
# locate source (necessary for Hedman, SkewPr)
my $program_dir;

my @dirs=split(/\//,$_);
pop(@dirs);
pop(@dirs);

$program_dir=join('/', @dirs);
$options{P}=$program_dir.'/';

# ----
Constraints(\%options);

# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$stfas=$_;
}

BEGIN {

	use lib $stfas;
	use ConfidenceIntervals::Constraints;

}
