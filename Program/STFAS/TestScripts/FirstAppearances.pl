#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# FirstAppearances.pl
# test function: FirstAppearances
# package: STFAS ConfidenceIntervals::
# other functions required: ReadTree, ReadOccurrences

my %options=();

getopts("a:dr:y:v", \%options);

# function options
if (!$options{y}) {
	die "Specify the treefile using -y!\n";
}
if (!$options{r}) {
	die "Specify the resolution using -r!\n";
}
if (!$options{a}) {
	die "Specify the maximum age of the basin using -a!\n";
}

# use $options{v} or $options{d} to switch on verbose mode

FirstAppearances(\%options);

# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$stfas=$_;
}

BEGIN {

	use lib $stfas;
	use ConfidenceIntervals::FirstAppearances;

}
