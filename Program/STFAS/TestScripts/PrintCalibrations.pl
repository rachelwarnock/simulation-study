#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# PrintCalibrations.pl
# test function: PrintCalibrations
# package: STFAS ConfidenceIntervals::
# other functions required: ReadTree, ReadOccurrences, FirstAppearances & MinimaOnly | BestPractice | Marshall | Hedman

my %options=();

getopts("a:dr:u:vy:", \%options);

# function options (PrintCalibrations)
if (!$options{y}) {
	die "Specify the treefile using -y!\n";
}
if (!$options{u}) {
	die "Specify the upper bound using -u!\n";
}
# additional function options
if (!$options{r}) {
	die "Specify the resolution using -r!\n";
}
if (!$options{a}) {
	die "Specify the maximum age of the basin using -a!\n";
}

# ----
# chose one of the following to generate min and max bounds:
# MinimaOnly | BestPractice | Marshall | Hedman
my $i;

my @temp=BestPractice(\%options);

$i=$temp[0];
my %minimum_bounds=%$i;
$i=$temp[1];
my %maximum_bounds=%$i;
# ----
# print calibrations

PrintCalibrations(\%options,\%minimum_bounds,\%maximum_bounds);

# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$stfas=$_;
}

BEGIN {

	use lib $stfas;
	use ConfidenceIntervals::MinimaOnly;
	use ConfidenceIntervals::BestPractice;
	use ConfidenceIntervals::Marshall;
	use ConfidenceIntervals::Hedman::Hedman;
	use ConfidenceIntervals::PrintCalibrations;

}
