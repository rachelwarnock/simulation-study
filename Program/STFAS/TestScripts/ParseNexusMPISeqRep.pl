#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# ParseNexusMPISeqRep.pl
# test function: ParseNexusMPISeqRep.pm
# package: STFAS
# other functions required: none

my %options=();

getopts("di:n:S:t:v", \%options);

# function options
if (!$options{t}) {
	die "Specify the number of trees using -t!\n";
}
if (!$options{n}) {
	die "Specify the number of taxa using -n!\n";
}
if (!$options{S}) {
	die "Specify the number of sequence replicates using -S!\n"; # default = 1
}

$options{i}="simulated_trees.nex" unless defined $options{i};

# use $options{v} or $options{d} to switch on verbose mode

ParseNexusMPISeqRep(\%options);

# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$stfas=$_;
}

BEGIN {

	use lib $stfas;
	use ParseNexusMPISeqRep;

}
