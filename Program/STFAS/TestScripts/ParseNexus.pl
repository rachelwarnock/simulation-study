#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# ParseNexus.pl
# test function: ParseNexus.pm
# package: STFAS
# other functions required: none

my %options=();

getopts("di:n:t:v", \%options);

# function options
if (!$options{t}) {
	die "Specify the number of trees using -t!\n";
}
if (!$options{n}) {
	die "Specify the number of taxa using -n!\n";
}

$options{i}="simulated_trees.nex" unless defined $options{i};

# use $options{v} or $options{d} to switch on verbose mode

ParseNexus(\%options);

# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$stfas=$_;
}

BEGIN {

	use lib $stfas;
	use ParseNexus;

}
