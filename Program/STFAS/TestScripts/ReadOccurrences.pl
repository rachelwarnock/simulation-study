#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# ReadOccurrences.pl
# test function: ReadOccurrences
# package: STFAS
# other functions required: none

my %options=();

getopts("a:r:vd", \%options);

# function options
if (!$options{r}) {
	die "Specify the resolution using -r!\n";
}

# verbosity - must be defined
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

ReadOccurrences($verbosity);

# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$stfas=$_;
}

BEGIN {

	use lib $stfas;
	use ConfidenceIntervals::ReadOccurrences;

}
	