#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# SimulateTrees.pl
# test function: SimulatesTrees.pm
# package: STFAS
# other funtions required: Extinction
# requires R (TreeSim libray) & dependencies

my %options=();

getopts("a:de:g:n:t:v", \%options);

# function options
if (!$options{n}) {
	die "Specify the number of taxa using -n!\n";
}
if (!$options{t}) {
	die "Specify the number of trees using -t!\n";
}
if (!$options{g}) {
	die "Specify the speciation rate using -g!\n";
}
if (!$options{e}) {
	die "Specify the extinction rate using -e!\n";
}
if (!$options{a}) {
	die "Specify the age of the root using -a!\n";
}

# simulated trees file
$options{i}='simulated_trees.nex' unless defined $options{i};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

SimulateTrees(\%options);

# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$stfas=$_;
}

BEGIN {

	use lib $stfas;
	use SimulateTrees;
	use Extinction;

}
