#!/usr/bin/perl
use strict;
use ReadTree;
use warnings;
use ConfidenceIntervals::PrintTree;

# this function prints out a tree for that can be read in R, given the minimum and maximum constraints
# required for use with the function SkewPr

sub PrintTreeR {

my $i=$_[0];
my %options=%$i;

$i=$_[1];
my %minimum_bounds=%$i;
$i=$_[2];
my %maximum_bounds=%$i;

# required input
my $treefile=$options{y};
my $upper_bound=$options{u};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
# create a label for all output files
my ($current_tree_abbrev, $txt) = split(/\./, $treefile);

# ----
# ReadTree - subroutine written by Julian Gough
my @temp=ReadTree($treefile,$verbosity); # subroutine returns (\%nodeup,\%distances,\%nodedown);

$i=$temp[0]; # first parameter is stored in $temp[0] 
my %parent=%$i; # place %nodeup in hash %parent

$i=$temp[2]; # third parameter is stored in $temp[2]
my %childs=%$i; # place %nodedown in hash %childs

# ----
my %nodelabels_for_R; # this loop requires something for non-calibrated nodes

foreach my $node (keys %childs) { # begin loop 1
	
	if ($verbosity==1) {
		print "Looking at node $node\n";
	}
	
	# for all internal nodes (e.g. not the root)
	if (exists($parent{$node})) { # begin loop 2
		
		if ($verbosity==1) {
			print "Node $node is not the root\n";
		}
		
		# if the node has a maximum
		if (exists($maximum_bounds{$node})) { # begin loop 3
		
			# and a minimum
			if (exists($minimum_bounds{$node})) { # begin loop 4
	
				$nodelabels_for_R{$node}="$minimum_bounds{$node}-$maximum_bounds{$node}";
		
				if ($verbosity ==1) {
					print "Maximum bounds for node $node : $maximum_bounds{$node}\n";
					print "Minimum bounds for node $node : $minimum_bounds{$node}\n";
					print "R nodelables for node $node : $nodelabels_for_R{$node}\n";		
				}

			} # end loop 4
			
			# else node has a maximum bound only
			else { # begin loop 5
			
				$nodelabels_for_R{$node}="0-$maximum_bounds{$node}";
			
			} # end loop 5					
		} # end loop 3
		
		# else if node has a minimum bound, but no maximum bound
		elsif (exists($minimum_bounds{$node})) { # begin loop 7a
		
			$nodelabels_for_R{$node}="$minimum_bounds{$node}-Inf";
			
			if ($verbosity==1) {
				print "Maximum bounds for node $node : don't exist\n";
				print "Minimum bounds for node $node : $minimum_bounds{$node}\n";
				print "nodelables for R node for $node : $nodelabels_for_R{$node}\n";
			}
			
		} # end loop 7a
		
		# else there are no contraints
		else { # begin loop 7b
			
			$nodelabels_for_R{$node}="0-Inf";
			
			if ($verbosity==1) {
				print "there are no constraints for node $node\n";
				print "nodelables for R node for $node : $nodelabels_for_R{$node}\n\n";
			}			
		} # end loop 7b
	} # end loop 2
	
	# for the root
	else { # begin loop 8

		if ($verbosity==1) {
			print "node $node is the root\n";
		}
		
		# if the root has a minimum bound
		if (exists($minimum_bounds{$node})) { # begin loop 9
			
			# and a maximum bound
			if (exists($maximum_bounds{$node})) { # begin loop 10
			
				# if the maxmimum bound is less than the upper bound
				if ($maximum_bounds{$node} < $upper_bound) { # begin loop 11

					$nodelabels_for_R{$node}="$minimum_bounds{$node}-$maximum_bounds{$node}";
					
					if ($verbosity==1) {
						print "the root has a min and max bound\n";
						print "and the maximum bound is < than the upper bound\n";
						print "nodelables for R node for $node : $nodelabels_for_R{$node}\n\n";
					}
					
				} # end loop 11
			
				# if the maximum is greater than the upper bound, use the specified maxima to constrain the root
				else { # begin loop 12

					$nodelabels_for_R{$node}="$minimum_bounds{$node}-$upper_bound";

					if ($verbosity==1) {
						print "the root has a min and max bound\n";
						print "but the maximum bound is > than the upper bound\n";
						print "nodelables for R node for $node : $nodelabels_for_R{$node}\n\n";
					}				
				
				} # end loop 12
			} # end loop 10
		
			# otherwise
			# if the root has no maximum - use the specified upper bound
			else { # being loop 13
		
				$nodelabels_for_R{$node}="$minimum_bounds{$node}-$upper_bound";
				
				if ($verbosity==1) {
					print "the root has a minimum but no maximum bound\n";
					print "nodelables for R node for $node : $nodelabels_for_R{$node}\n\n";
				}				
		
			} # end loop 13
		} # end loop 9
		
		# if the root has a minimum and no maximum
		else { # begin loop 14
		
			$nodelabels_for_R{$node}="0-$upper_bound";
			
		} # end loop 14	
	} # end loop 8
	
	if ($verbosity==1) {
		print "\n";
	}
} # end loop 1

# ----
# PrintTree - subrountine written by Julian Gough
# obtain the newick string for the R tree
my $newicktree_R=PrintTree(\%parent,\%nodelabels_for_R,\%childs);
if ($verbosity ==1) {
	print "The tree for R looks like this:\n", "$newicktree_R\n\n";
}

# ----
# Print tree file for R

open (RTREE, ">R.$current_tree_abbrev.txt") || die "can't open file\n";
print RTREE "$newicktree_R\n";
close RTREE;

# ----

# end of sub routine PrintTreeR

}

1;

