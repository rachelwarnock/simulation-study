#!/usr/bin/perl
use strict;
use warnings;

# this function returns a newick format tree string given the hash structure output by the function ReadTree
# This subroutine was written by Julian Gough, University of Bristol

sub PrintTree {

my ($i,$newick,$j,$flag); # define variables
$i=$_[0]; # first argument passed to the subroutine c.f. my $newicktree=&PrintTree(\%parent,\%nodelabels,\%childs);
my %parent=%$i; # place %parent in hash %parent
$i=$_[1]; # second argument passed to the subroutine
my %distances=%$i; # place %nodelabels in hash %distances
$i=$_[2]; # third argument passed to the subroutine
my %childs=%$i; # place %childs in hash %childs
my %gens;
my (@temp,@temp2);

# for each node and terminal
foreach $i (keys(%parent)) {
	
  
  unless ($i =~ /:/) {
  # identify terminal nodes	
  	if (exists($distances{$i})){
  		$gens{$i}="$i'$distances{$i}'";
  		# terminals should not have any nodelabels
  	}
  	else {
  		$gens{$i}="$i";
  		# place terminals in hash gens
  	}
  }
}
# ----

#join them all up until no more joining to be done
until (scalar(keys(%gens)) <= 1) {
	# until the contents of hash gens is less than or equal to 1

	foreach $i (keys(%gens)) {
		# for each terminal

		if (exists($gens{$i})){
			# if terminal exists
			
			@temp = split /,/,$childs{$parent{$i}};
			# split the children of the parent node of the terminal by ','
			# place in array @temp
			$flag=1;
			@temp2=();
			
			foreach $j (@temp) {
				unless (exists($gens{$j})) {
					$flag=0;
				}
				else{
					push @temp2,$gens{$j};
				}
			}
			
			if ($flag == 1) {
				$gens{$parent{$i}}=join ',',@temp2;
				if (exists($distances{$parent{$i}})) {
					$gens{$parent{$i}}="($gens{$parent{$i}})'$distances{$parent{$i}}'";
					# create nodelabels
					# 
				}
				
				else {
					$gens{$parent{$i}}="($gens{$parent{$i}})";
				}
				foreach $j (@temp) {
					delete($gens{$j});
					$newick=$gens{$parent{$i}}.';';
				}
			}
		}
	}
}

# ----

return ($newick);

}

1;
