#!/usr/bin/perl
use strict;
use warnings;
use ReadTree;
use ConfidenceIntervals::FirstAppearances;
use ConfidenceIntervals::ReadOccurrences;

sub ArbitraryMax {

my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{y};
my $resolution=$options{r};
my $max_age=$options{a};
my $arbitrary_factor=$options{U};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
#ReadTree - subroutine written by Julian Gough
my @temp=ReadTree($treefile,$verbosity); # subroutine returns (\%nodeup,\%distances,\%nodedown);

$i=$temp[0]; # first parameter is stored in $temp[0] 
my %parent=%$i; # place %nodeup in hash %parent

$i=$temp[2]; # third parameter is stored in $temp[2]
my %childs=%$i; # place %nodedown in hash %childs

# ----
# obtain the minimum bounds
@temp=FirstAppearances(\%options);
$i=$temp[0];
my %minimum_bounds=%$i; # place %occurrences in hash %minimum_bounds

# ----
# read occurrence data
@temp=ReadOccurrences(\%options);
$i=$temp[0];
my %occurrences=%$i; # place %occurrences in hash %occurrences

# ----
# work out the number of decimal places to use
# using the same rounding strategy used buy the functions SimUnifOccurrences & SimNonUnifOccurrences
my $sig_figs=length($resolution);
my $sprintf="%.$sig_figs"."f";

if ($verbosity ==1) {
	print "The resolution is $resolution (number of horizons)\n";
	print "sprintf is $sprintf\n";
}

# ----
# work out the maximum age of each horizon
# using the same rounding strategy used buy the functions SimUnifOccurrences & SimNonUnifOccurrences
my %horizon_max;
my $current_horizon;
my $previous_horizon=0;

foreach my $horizon (1..$resolution) {

	# work the horizon ID (equivalent to the maximum horizon age)
	$current_horizon=($horizon/$resolution) * $max_age;
	
	# round to the nearest decimal place (specified above)
	$current_horizon=sprintf("$sprintf", $current_horizon);

	# store the maximum age of the horizon
	$horizon_max{$previous_horizon}=$current_horizon;
	
	$previous_horizon=$current_horizon;
	
}

# ----
# Defining maxima

my $max_nodes=5;

my %maximum_bounds;
my $minimum_bound;
my $maximum_bound;
my $oldest_secure;

foreach my $node (keys %childs) { # begin loop 1
	
	if ($verbosity == 1) {
		print "Looking at node : $node\n";
	}
	
	
	if (exists($minimum_bounds{$node})) {  # begin loop 3a
	# if the node already has a minimum bound
	# NB. this function will not generate a maximum constraint for a node that doesn't already have a minimum
		
			$minimum_bound=$minimum_bounds{$node};

			$minimum_bound=sprintf("$sprintf", $minimum_bound);
			
			unless ($minimum_bound==0) {
							
				$oldest_secure=$horizon_max{$minimum_bound};
				# get the old secure age of the horizon				
				
				$maximum_bound=$oldest_secure*$arbitrary_factor;
				# multiply by the arbitrary factor
			
				$maximum_bounds{$node}=sprintf("$sprintf", $maximum_bound);
				
				# keep track the minimum bounds of parent nodes
				if ($verbosity == 1) {
					print "node $node has minimum constraint : $minimum_bounds{$node}\n";
					print "taking the oldest secure age of this horizon (= $oldest_secure), and multiplying by $arbitrary_factor...\n";
					print "node $node has a maximum constraint : $maximum_bounds{$node}\n";
				}
			}			
		 	
		} # end loop 3a
	
	if ($verbosity==1) {
		print "…finished looking at node $node\n\n";
	}
	
} # end loop 1
	
# ----	
return(\%minimum_bounds,\%maximum_bounds);

# end of subroutine
	
}

1;