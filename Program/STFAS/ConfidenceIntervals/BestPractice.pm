#!/usr/bin/perl
use strict;
use warnings;
use ReadTree;
use ConfidenceIntervals::FirstAppearances;

# this function works out the minimum and maximum constraints based on a proxy for "best practice" approaches (= phylogenentic bracketing)
# minima are based on the (oldest secure) first appearances
# maxima are defined by the minima of parent nodes
# this function will not use the minimum of a parent node unless it is from an older horizon than the minimum of the given node
# minimum and maximum bounds are stored and returned in individual hashes

sub BestPractice {

my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{y};
my $resolution=$options{r};
my $max_age=$options{a};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
#ReadTree - subroutine written by Julian Gough
my @temp=ReadTree($treefile,$verbosity); # subroutine returns (\%nodeup,\%distances,\%nodedown);

$i=$temp[0]; # first parameter is stored in $temp[0]
my %parent=%$i; # place %nodeup in hash %parent

$i=$temp[2]; # third parameter is stored in $temp[2]
my %childs=%$i; # place %nodedown in hash %childs

# ----
# obtain the minimum bounds
@temp=FirstAppearances(\%options);
$i=$temp[0];
my %minimum_bounds=%$i; # place the minima in hash %minimum_bounds

# ----
# work out the maximum age of each horizon
# using the same rounding strategy used buy the functions SimUnifOccurrences & SimNonUnifOccurrences
my $sig_figs=length($resolution);
my $sprintf="%.$sig_figs"."f";

my %horizon_max;
my $current_horizon;
my $previous_horizon=0;

foreach my $horizon (1..$resolution) {

	# work the horizon ID (equivalent to the maximum horizon age)
	$current_horizon=($horizon/$resolution) * $max_age;
	
	# round to the nearest decimal place (specified above)
	$current_horizon=sprintf("$sprintf", $current_horizon);

	# store the maximum age of the horizon
	$horizon_max{$previous_horizon}=$current_horizon;
	
	$previous_horizon=$current_horizon;
	
}

# ----
# calculate the number of terminals & the number of nodes
# required to define the maximum number of times loop 5 looks for a parent node
my $terminals=0;
foreach my $terminal (keys %parent) {
	unless ($terminal =~/:/) {
		$terminals=$terminals+1;
	}
}

my $max_nodes=$terminals-2;

# ----
# defining maximum bounds using phylogenetic bracketing
# this loop define maximum bounds based on the minima of parent nodes
my $horizon_length=0.01;
my %maximum_bounds;
my $minimum_bound;
my $oldest_secure;

foreach my $node (keys %childs) { # begin loop 1
	
	if ($verbosity == 1) {
		print "Looking at node : $node\n";
	}

	if (exists($parent{$node})) { # begin loop 2
	# if that node has a parent node
	
		my $parent = $parent{$node};
		# define the current parent
			
		# keep track of parent/child node IDs
		if ($verbosity ==1) {
			print "parent of node $node : $parent \n";
		}
		
		if (exists($minimum_bounds{$node})) {  # begin loop 3a
		# if the node already has a minimum bound
		# NB. this function will not generate a maximum constraint for a node that doesn't already have a minimum
		
			if ((exists($minimum_bounds{$parent})) && ($minimum_bounds{$parent} > $minimum_bounds{$node})) { # begin loop 3
			# if that parent has a minimum bound
			# and the parent minimum bound is > than the node minimum bound

				$minimum_bound=$minimum_bounds{$parent};
				
				$minimum_bound=sprintf("$sprintf", $minimum_bound);
				
				$oldest_secure=$horizon_max{$minimum_bound};
				# get the old secure age of the horizon
				
				$maximum_bounds{$node}=$oldest_secure;
				# define this as the maximum bound of the current node
				
				# keep track the minimum bounds of parent nodes
				if ($verbosity == 1) {
					print "node $node has parent $parent with minimum constraint : $minimum_bounds{$parent}\n";
					print "taking the oldest secure age of this horizon...\n";
					print "node $node has a maximum constraint : $maximum_bounds{$node}\n";
				}
				
			} # end loop 3
		 	else { # begine loop 4
		 	
		 		foreach my $i (1..$max_nodes) { # begin loop 5
		 		
		 			if ($verbosity == 1) {
		 				print '$i : ', "$i\n";
		 			}
		 		
		 			unless (exists($maximum_bounds{$node})) { # begin loop 6
		 			# unless a maximum bound already exists
		 			
		 				if (exists($parent{$parent})) { # begin loop 7
		 				# if the parent node has a parent
		 				
		 					$parent = $parent{$parent}; # define this as the new parent
		 				
		 					if ($verbosity == 1) {
				 				print "parent of previous parent : $parent\n";
		 					}
							if ((exists($minimum_bounds{$parent})) && ($minimum_bounds{$parent} > $minimum_bounds{$node})) { # begin loop 8
							# if that parent has a minimum bound
							# if that minimum bound is > than the minimum bound of the current node
							
								if ($verbosity == 1) {
									print "Found a minimum!!\n";
								}
								
								$minimum_bound=$minimum_bounds{$parent};
								
								# specify the correct number of decimals used to define the age of the horizon
								$minimum_bound=sprintf("$sprintf", $minimum_bound);
				
								$oldest_secure=$horizon_max{$minimum_bound};
								# get the old secure age of the horizon
				
								$maximum_bounds{$node}=$oldest_secure;
								# define this as the maximum bound of the current node
							
								if ($verbosity == 1) {
									print "node $node has ancestor $parent with minimum constraint : $minimum_bounds{$parent}\n";
									print "taking the oldest secure age of this horizon...\n";
									print "node $node has a maximum constraint : $maximum_bounds{$node}\n";
								}
							
							} # end loop 8
		 				} # end loop 7
		 			} # end loop 6
		 		} # end loop 5
			} # end loop 4
		} # end loop 3a
	} # end loop 2
	
	if ($verbosity==1) {
		print "…finished looking at node $node\n\n";
	}
	
} # end loop 1

#---------

return(\%minimum_bounds,\%maximum_bounds);
#PrintTreeMCMCTREE(\%options,\%minimum_bounds,\%maximum_bounds);

# end of subroutine Benton

}

1;
