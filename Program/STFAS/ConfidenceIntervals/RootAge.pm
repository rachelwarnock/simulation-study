#!/usr/bin/perl
use strict;
use warnings;
use BranchingTimes;

# Rootage
# this function returns a value for the upper bound
# by taking an optional multiple of the true root age

sub RootAge {

my $i=$_[0];
my %options=%$i;

# input 
my $upper_multiple=$options{u};

# ----
# obtain the true ages
my(%parent,%true_ages,%childs);
my @temp=BranchingTimes(\%options);
$i=$temp[0];
%parent=%$i;
$i=$temp[1];
%true_ages=%$i;
$i=$temp[2];
%childs=%$i;

# ----
my $root_age;
foreach my $node (keys %childs) {
	# for the root node
	unless (exists($parent{$node})) {
		# root age
		$root_age=$true_ages{$node};
	}
}
# ----
# define the upper bound
my $upper_bound=$upper_multiple*$root_age;
$upper_bound=sprintf("%.4f", $upper_bound);
return($upper_bound);

# ----
}

1;