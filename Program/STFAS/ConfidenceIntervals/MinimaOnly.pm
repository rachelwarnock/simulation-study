#!/usr/bin/perl
use strict;
use warnings;
use ConfidenceIntervals::FirstAppearances;

# function returns minimum & maximum constraints stored in individual hashes
# note this function returns an empty hash for maximum constraints
# minima are based on first appearances
# the minima matches the "oldest secure" age of the fossil = the minimum age of the horizon in which the fossil was found

sub MinimaOnly {

my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{y};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
my @temp=FirstAppearances(\%options);
$i=$temp[0];
my %minimum_bounds=%$i; # place %occurrences in hash %minimum_bounds
my %maximum_bounds;

# ----

return(\%minimum_bounds,\%maximum_bounds);

# end of subroutine minima only

}

1;
