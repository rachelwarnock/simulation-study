#!/usr/bin/perl
use strict;
use warnings;
use ReadTree;
use ConfidenceIntervals::FirstAppearances;
use ConfidenceIntervals::ReadOccurrences;

# this function generates maximum bounds using equation 11 & 14. in Marshall (2008) American Naturalist; vol. 171, no. 6
# A Simple Method for Bracketing Absolute Divergence Times on Molecular Phylogenies Using Multiple Fossil Calibration Points
# using a conservative approach that assumes the number horizons can not be calculated
# for derivation see Appendix A of this paper
# minimum bounds are based on first appearances
# minimum and maximum bounds are stored and returned in individual hashes

sub MarshallConservative {

my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{y};
my $resolution=$options{r};
my $max_age=$options{a};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
#ReadTree - subroutine written by Julian Gough
my @temp=ReadTree($treefile,$verbosity); # subroutine returns (\%nodeup,\%distances,\%nodedown);

$i=$temp[0]; # first parameter is stored in $temp[0] 
my %parent=%$i; # place %nodeup in hash %parent

$i=$temp[2]; # third parameter is stored in $temp[2]
my %childs=%$i; # place %nodedown in hash %childs

# ----
# obtain the minimum bounds
@temp=FirstAppearances(\%options);
$i=$temp[0];
my %minimum_bounds=%$i; # place %occurrences in hash %minimum_bounds

# ----
# read occurrence data
@temp=ReadOccurrences(\%options);
$i=$temp[0];
my %occurrences=%$i; # place %occurrences in hash %occurrences

# ----
# work out the number of decimal places to use
# using the same rounding strategy used buy the functions SimUnifOccurrences & SimNonUnifOccurrences
my $sig_figs=length($resolution);
my $sprintf="%.$sig_figs"."f";

# check this calculation (the correct specification of decimal places) is correct 
if ($verbosity ==1) {
	print "The resolution is $resolution (number of horizons)\n";
	print "sprintf is $sprintf\n";
}

# ----
# work out the maximum age of each horizon
# using the same rounding strategy used buy the functions SimUnifOccurrences & SimNonUnifOccurrences
my %horizon_max;
my $current_horizon;
my $previous_horizon=0;

foreach my $horizon (1..$resolution) {

	# work the horizon ID (equivalent to the maximum horizon age)
	$current_horizon=($horizon/$resolution) * $max_age;
	
	# round to the nearest decimal place (specified above)
	$current_horizon=sprintf("$sprintf", $current_horizon);

	# store the maximum age of the horizon
	$horizon_max{$previous_horizon}=$current_horizon;
	
	$previous_horizon=$current_horizon;
	
}

# ----
# Defining maxima
# see equation 11 and 14. Marshall (2008) American Naturalist; vol. 171, no. 6

my %maximum_bounds;
my $confidence_limits=0.95;
my %combined_occurrences;
my $oldest_occurrence;

foreach my $node (keys %childs) { # begin loop 1
	# for each node in the tree
	
	if ($verbosity ==1) {
		print "this is node $node\n";
	}
	
	my @lineages = split(',', $childs{$node});
	
	if ($verbosity ==1) {
		print "lineage 1 for node $node : $lineages[0]\n";
		print "lineage 2 for node $node : $lineages[1]\n";
	}
	
	# Case A: both lineages have occurrence data
	if (exists($occurrences{$lineages[0]}) && exists($occurrences{$lineages[1]})) { # begin loop 2
		# check if occurrence data exists for both lineages
		
		if ($verbosity ==1) {
			print "this is case A\n";
			print "both lineages have occurrence data\n";
		}
		
		# combine occurrence data
		foreach my $data (@{$occurrences{$lineages[0]}}) {	
			push @{$combined_occurrences{$node}}, $data;
		}
		foreach my $data (@{$occurrences{$lineages[1]}}) {
			push @{$combined_occurrences{$node}}, $data;
		}
		
		@{$combined_occurrences{$node}}=&uniq(@{$combined_occurrences{$node}});
		# remove duplicate occurrences
	
		@{$combined_occurrences{$node}}=sort@{$combined_occurrences{$node}};
		# sort numerically (probably not necessary)
		
		my $number_finds = scalar@{$combined_occurrences{$node}};
		# define the total number of occurrences
		
		if ($verbosity ==1) {
			print "number of finds for $node : $number_finds", "\n";
		}
		
		if (scalar@{$combined_occurrences{$node}} > 1) { # begin loop 4 # should this be 2
		# check if the number of occurrences is greater than 1	
			
			my $lineage_number= scalar @lineages;
			# define the number of occurrences
			
			my $minima = sprintf("$sprintf", $minimum_bounds{$node});
			# define the minima
			
			my $oldest_occurrence = $horizon_max{$minima};
			#  N.B must be oldest secure - this means this should really be oldest secure age of the horizon from which fossil
			# was found
			
			my $number_finds=1;
			
			my $maximum_divergence = $oldest_occurrence/ ((1-$confidence_limits)**(1/($lineage_number*$number_finds)));
			# calculate the maximum bound based on Marshall (2008) eq. 11.			
			
			my $maximum_divergence_round = sprintf "$sprintf", $maximum_divergence; # this needs to be changed depending on the resolution
			
			if ($verbosity ==1) {
				print "maximum for $number_finds for $node is = $maximum_divergence_round \n";
			}
			
			$maximum_bounds{$node}=$maximum_divergence_round;
			
		} # end loop 4

	} # end loop 2	
	
	# Case B: only lineage 1. has occurrence data
	elsif (exists($occurrences{$lineages[0]})) { # begin loop 7
	
		if ($verbosity ==1) {
			print "this is case B\n";
		}
		
		my $number_finds = scalar@{$occurrences{$lineages[0]}};
		
		if ($verbosity ==1) {
			print "number of finds for node $node : $number_finds", "\n";
		}
		
			if (scalar@{$occurrences{$lineages[0]}} > 1) { # begin loop 8			
				
				my $minima = sprintf("$sprintf", $minimum_bounds{$node});
				# define the minima
			
				my $oldest_occurrence = $horizon_max{$minima};
				# use oldest secure				
				
				my $number_finds=1;
				
				my $maximum_divergence = $oldest_occurrence/ ((1-$confidence_limits)**(1/(1*$number_finds)));
				
				my $maximum_divergence_round = sprintf "$sprintf", $maximum_divergence;
				
				if ($verbosity ==1) {
					print "maximum for number of finds : $number_finds for $node is = $maximum_divergence_round \n";
				}
				
				$maximum_bounds{$node}=$maximum_divergence_round;
				
			} # end loop 8
	} # end loop 7
	
	# Case C: only lineage 2. has occurrence data
	elsif (exists($occurrences{$lineages[1]})) { # begin loop 9
	
		if ($verbosity ==1) {	
			print "this is case C\n";
		}
				
		my $number_finds = scalar@{$occurrences{$lineages[1]}};
		
		if ($verbosity ==1) {
			print "number of finds for $node : $number_finds", "\n";
		}
		
		if (scalar@{$occurrences{$lineages[1]}} > 1) {	# begin loop 10	

			my $minima = sprintf("$sprintf", $minimum_bounds{$node});
			# define the minima
			
			my $oldest_occurrence = $horizon_max{$minima};
			
			my $number_finds=1;
			
			my $maximum_divergence = $oldest_occurrence/ ((1-$confidence_limits)**(1/(1*$number_finds)));
			
			my $maximum_divergence_round = sprintf "$sprintf", $maximum_divergence;

			if ($verbosity ==1) {
				print "maximum for numner of finds : $number_finds for $node is = $maximum_divergence_round \n";
			}

			$maximum_bounds{$node}=$maximum_divergence_round;
			
		} # end loop 10
	} # end loop 9
	
	# Part D
	else { # begin loop 11
	
		if ($verbosity ==1) {		
			print "this is case D\n";
			print "there is no fossil data for $node\n\n";
		}
		
	} # end loop 11
} # end loop 1

# ----
return(\%minimum_bounds,\%maximum_bounds);

# end of subroutine Marshall

}


1;