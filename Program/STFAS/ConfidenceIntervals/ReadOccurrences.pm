#!/usr/bin/perl
use strict;
use warnings;

# this function reads occurrence data in the format output by SimUnifOccurrences or SimNonUnifOccurrence and creates a hash containing the occurrence data
# N.B It is extremely important to note that this function simulates occurrence data along stem lineages and that these branches are labelled by all descendant terminals
# This is contrast to most other functions in this package, which use an identical labelling scheme to refer to nodes
# the function has some limited ability to produce wanrings if the data was simualted under different conditions than those specified

sub ReadOccurrences {

my $i=$_[0];
my %options=%$i;

# required input
my $resolution=$options{r};
my $max_age=$options{a};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
# to check the specified resolution was correct work out the name of each horizon and create a hash of IDs
# using the same rounding strategy used buy the functions SimUnifOccurrences & SimNonUnifOccurrences
my $sig_figs=length($resolution);
my $sprintf="%.$sig_figs"."f";
my %horizons;

# create a hash that contains the horizon IDs
foreach my $horizon (1..$resolution) {
	$horizon=($horizon/$resolution) * $max_age;
	$horizon=sprintf("$sprintf", $horizon);
	$horizons{$horizon}=$horizon;
} 

# ----
# read occurrence data
open (OCC, "Occurrence_data.txt") || die "can't open file";

my @lines = <OCC>;

my ($lineage, $fossils);
my %occurrences;

foreach my $lines(@lines) {
	if ($lines =~ m/^t/ ) {
		
		#track the input file
		if ($verbosity ==1) {
			print $lines;
		}
		
		($lineage, $fossils) = split(/: /, $lines);
		
		# track the lineages
		if ($verbosity ==1) {
			print $lineage, "\n";
		}
		
		chomp $fossils;
		
		# track the fossils
		if ($verbosity ==1) {			
		print $fossils, "\n";
		}
		
		my @fossil_array = split(/ /, $fossils);

		foreach my $fossil (@fossil_array) {
			
			# is the occurrence data being read correctly?
			if ($verbosity ==1) {
				print "fossil recorded at horizon: $fossil\n";
			}
		
			unless (exists($horizons{$fossil})) {				
				die "something went wrong when you defined the resolution (-r) and/or maximum age (-a)!\nnot the same as the parameters used to simualte the data\n";
			}
				
			push @{$occurrences{$lineage}}, $fossil;
		}
	}
}

# ----

# examine the structure of the hash 'occurrences'
if ($verbosity == 1) {
	for my $occurrence (keys %occurrences) {
		print "$occurrence: @{$occurrences{$occurrence}}\n";
	}

	print "# ----\n";
}

close OCC;

return(\%occurrences);

# end subroutine ReadOccurrenes

}

1;