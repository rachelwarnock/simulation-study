#!/usr/bin/perl
use strict;
use warnings;
use ConfidenceIntervals::RootAge;
use ConfidenceIntervals::BestPractice;
use ConfidenceIntervals::Marshall;
use ConfidenceIntervals::MarshallConservative;
use ConfidenceIntervals::MinimaOnly;
use ConfidenceIntervals::Hedman::Hedman;
use ConfidenceIntervals::PrintTreeMCMCTREE;
use ConfidenceIntervals::PrintTreeR;
use ConfidenceIntervals::SkewPr::SkewPr;
use ConfidenceIntervals::PrintCalibrations;
use ConfidenceIntervals::ArbitraryMax;

# this function generates constraints using any of the following appraoches:
# MinimaOnly | BestPractice | Marshall | Marshall (conservative) | Hedman
# and prints out uniform | non-uniform calibration priors for analysis in
# mcmctree

sub Constraints {

my $i=$_[0];
my %options=%$i;

# ----
# Redefine the upper bound
my $upper_bound=RootAge(\%options);
$options{u}=$upper_bound;
# ----
# required input
my $treefile=$options{y};	
my $constraint_type=$options{k};
my $priors=$options{j}; # uniform or non-uniform priors

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};
# ----

my @temp;
my %minimum_bounds;
my %maximum_bounds;

if ($constraint_type =~ m/^[B|b]$/ ) { # best practice appraoch
	if ($verbosity==1) {
		print "option B in action\n";
	}
	@temp=BestPractice(\%options);
}
elsif ($constraint_type =~ m/^[M|m]$/ ) { # Marshall
	if ($verbosity==1) {
		print "option M in action\n";
	}
	@temp=Marshall(\%options);
}
elsif ($constraint_type =~ m/^[A|a]$/ ) { # Arbitrary Max
	if ($verbosity==1) {
		print "option A in action\n";
	}
	@temp=ArbitraryMax(\%options);
}
elsif ($constraint_type =~ m/^[C|c]$/ ) { # Marshall (conservative)
	if ($verbosity==1) {
		print "option M (con) in action\n";
	}
	@temp=MarshallConservative(\%options);
}
elsif ($constraint_type =~ m/^[H|h]$/ ) { # Hedman
	if ($verbosity==1) {
		print "option H in action\n";
	}
	@temp=Hedman(\%options);
}
elsif ($constraint_type =~ m/^[L|l]$/ ) { # Minima only (Cauchy distirbution)
	if ($verbosity==1) {
		print "option L in action\n";
	}
	@temp=MinimaOnly(\%options);
}
# define the constraints
$i=$temp[0];
%minimum_bounds=%$i;
$i=$temp[1];
%maximum_bounds=%$i;

# ----
# print handy tree for later analysis
# at the moment the purpose of file is simply for generating SkewPr priors
# it's an easy way of getting the minimum and maximum bounds into R via ape
PrintTreeR(\%options,\%minimum_bounds,\%maximum_bounds);

# ----
# print tree for analysis in mcmctree
if ($priors =~ m/^[U|u]$/) {
	PrintTreeMCMCTREE(\%options,\%minimum_bounds,\%maximum_bounds);
}
elsif ($priors =~ m/^[N|n]$/) {
	SkewPr(\%options);
}

# ----
# print out the file necessary for later analysis
PrintCalibrations(\%options,\%minimum_bounds,\%maximum_bounds);



}

1;
