#!/usr/bin/perl
use strict;
use warnings;
use ReadTree;
use ConfidenceIntervals::PrintTree;

# this function prints out a tree for analysis mcmctree, given the minimum and maximum constraints

sub PrintTreeMCMCTREE {

my $i=$_[0];
my %options=%$i;

$i=$_[1];
my %minimum_bounds=%$i;
$i=$_[2];
my %maximum_bounds=%$i;

# required input
my $treefile=$options{y};
my $upper_bound=$options{u};
my $p=$options{p};
my $c=$options{c};
my $lower_tail=$options{m};
my $upper_tail=$options{b};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
# create a label for all output files
my ($current_tree_abbrev, $txt) = split(/\./, $treefile);

# ----
#ReadTree - subroutine written by Julian Gough
my @temp=ReadTree($treefile,$verbosity); # subroutine returns (\%nodeup,\%distances,\%nodedown);

$i=$temp[0]; # first parameter is stored in $temp[0] 
my %parent=%$i; # place %nodeup in hash %parent

$i=$temp[2]; # third parameter is stored in $temp[2]
my %childs=%$i; # place %nodedown in hash %childs

# ----

my %nodelabels_for_mcmctree;

foreach my $node (keys %childs) { # begin loop 1
	
	if ($verbosity==1) {
		print "Looking at node $node\n";
	}

	# for all internal nodes (e.g. not the root)
	if (exists($parent{$node})) { # begin loop 2

		if ($verbosity==1) {
			print "Node $node is not the root\n";
		}
		
		# if this node has a maximum
		if (exists($maximum_bounds{$node})) { # begin loop 3
			
			# and a minimum bound
			if (exists($minimum_bounds{$node})) { # begin loop 4
			
				$nodelabels_for_mcmctree{$node}="B($minimum_bounds{$node},$maximum_bounds{$node},$lower_tail,$upper_tail)";
		
				if ($verbosity ==1) {
				print "Maximum bounds for node $node : $maximum_bounds{$node}\n";
				print "Minimum bounds for node $node : $minimum_bounds{$node}\n";
				print "node label for mcmcmtree for node $node : $nodelabels_for_mcmctree{$node}\n";
				}
				
			} # end loop 4
			
			# else node has a maximum bound only
			else { # begin loop 5
				$nodelabels_for_mcmctree{$node}="U($maximum_bounds{$node},$upper_tail)";
			} # end loop 5		
		} # end loop 3
		
		# NB there is no loop 6
		
		# else if node has a minimum bound, but no maximum bound
		elsif (exists($minimum_bounds{$node})) { # begin loop 7
		
			$nodelabels_for_mcmctree{$node}="L($minimum_bounds{$node},$p,$c,$lower_tail)";
	
			if ($verbosity == 1) {
				print "Maximum bounds for node $node : don't exist\n";
				print "Minimum bounds for node $node : $minimum_bounds{$node}\n";
				print "node label for mcmctree for node $node : $nodelabels_for_mcmctree{$node}\n";
			}	
		} # end loop 7
		
		else {
			if ($verbosity==1) {
				print "there are no constraints for node $node\n";
			}
		}
		
	} # end loop 2
	
	# for the root
	else { # begin loop 8
	
		if ($verbosity==1) {
			print "node $node is the root\n";
		}
		
		# if the root has a minimum bound
		if (exists($minimum_bounds{$node})) { # begin loop 9
			
			# if the root also has a maximum bound
			if (exists($maximum_bounds{$node})) { # begin loop 10
				
				# if the maximum bound is less than the upper bound
				if ($maximum_bounds{$node} < $upper_bound) { # begin loop 11
				
					$nodelabels_for_mcmctree{$node}="B($minimum_bounds{$node},$maximum_bounds{$node},$lower_tail,$upper_tail)";
					
					if ($verbosity==1) {
						print "the root has a minimum and maximum bound\n";
						print "and maximum bound is < than the upper bound\n";
						print "mcmctree node label for the root is $nodelabels_for_mcmctree{$node}\n";
					}					
					
				} # end loop 11
				
				# if the maximum is greater than the upper bound, use the specified maxima to constrain the root
				else { # begin loop 12
				
					$nodelabels_for_mcmctree{$node}="B($minimum_bounds{$node},$upper_bound,$lower_tail,$upper_tail)";
					
					if ($verbosity==1) {
						print "the root has a minimum and maximum bound\n";
						print "but the maximum bound is > than the upper bound\n";
						print "mcmctree node label for the root is $nodelabels_for_mcmctree{$node}\n";
					}
					
				} # end loop 12
					
			} # end loop 10
			
			# otherwise
			# if the root has no maximum - use the specified upper bound
			else { # begin loop 13
			
				$nodelabels_for_mcmctree{$node}="B($minimum_bounds{$node},$upper_bound,$lower_tail,$upper_tail)";
				
				if ($verbosity==1) {
					print "the root has a minimum but no maximum bound\n";
					print "mcmctree node label for the root is $nodelabels_for_mcmctree{$node}\n";
				}
				
			} # end loop 13
		} # end loop 9
		
		# if the root has no minimum and no maximum
		else { # begin loop 14
			$nodelabels_for_mcmctree{$node}="U($upper_bound,$upper_tail)";
			
			if ($verbosity==1) {
				print "the root has no minimum or no maximum bound\n";
				print "mcmctree node label for the root is $nodelabels_for_mcmctree{$node}\n";
			}
			
		} # end loop 14
	} # end loop 8
	
	if ($verbosity==1) {
		print "\n";
	}
	
} # end loop 1

# ----
# PrintTree - subrountine written by Julian Gough
# obtain the newick string for the tree
my $newicktree=PrintTree(\%parent,\%nodelabels_for_mcmctree,\%childs);

if ($verbosity==1) {
	print "The tree for mcmctree looks like this:\n", "$newicktree\n\n";
}

# ----
# Print tree file for mcmctree

# calculate the number of terminals
my $terminals=0;
foreach my $terminal (keys %parent) {
	unless ($terminal =~/:/) {
		$terminals=$terminals+1;
	}
}

open (MCMCTREE, ">mcmctree.$current_tree_abbrev.txt") || die "can't open file";
print MCMCTREE "\t$terminals 1\n\n$newicktree\n";
close MCMCTREE;

# ----

# end of sub routine PrintTreeMCMCTREE

}

1;

