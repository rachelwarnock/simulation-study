#!/usr/bin/perl
use strict;
use warnings;
use ReadTree;
use ConfidenceIntervals::FirstAppearances;

# this sub routine (rename OutgroupFADS) gathers all the minimum bounds from parent nodes (outgroups)
# for the purpose of implementing Herman's conservative method
# Hedman - conservative approach
# "we recognise that because the assumed cladogram requires that the node leading taxon J-1 (the parent node)
# must be earlier that leading to J (the node), some member of J-1's clade existed as early as tJ and we can extend a
# ghost lineage... With the conservation approach we simply ignore taxon J-1('s FAD) and include only those
# fossil outgroups that follow the correct stratigraphic sequence"
# J also = N

sub HedmanConservative {

my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{y};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
#ReadTree - subroutine written by Julian Gough
my @temp=ReadTree($treefile,$verbosity); # subroutine returns (\%nodeup,\%distances,\%nodedown);

$i=$temp[0]; # first parameter is stored in $temp[0]
my %parent=%$i; # place %nodeup in hash %parent

$i=$temp[2]; # third parameter is stored in $temp[2]
my %childs=%$i; # place %nodedown in hash %childs

# ----
# gather the first appearances
@temp=&FirstAppearances(\%options);
$i=$temp[0];
my %minimum_bounds=%$i; # place %occurrences in hash %minimum_bounds

# ----
# calculate the number of terminals
# necessary to put a max on the number of times the loop below looks for parent nodes
my $terminals=0;
foreach my $terminal (keys %parent) {
	unless ($terminal =~/:/) {
		$terminals=$terminals+1;
	}
}
my $max_nodes=$terminals-2;

# ----
# defining outgroup FADs for the purposes of implementing Hedman (2010)

# this hash contains the FADS (minimum bounds) of all parent nodes...
# ... for each node PLUS the FAD (minimum bound) of that node
my %outgroup_FADS; 

my $previous_minimum_bound; # this is for keeping track of previous parent node minima (see loop 7)

foreach my $node (keys %childs) { # begin loop 1

	if ($verbosity == 1) {
		print "Looking at node : $node\n";
	}
	
	if (exists($minimum_bounds{$node})) { # begin loop 1b
	# if node has a minimum bound	
		
		# collect the minimum bound for that node
		push @{$outgroup_FADS{$node}}, $minimum_bounds{$node};
			
		if ($verbosity == 1) {
			print "node $node has minimum constraint : $minimum_bounds{$node}\n";
		}

		if (exists($parent{$node})) { # begin loop 2
	
			my $parent = $parent{$node};
			
			# keep track of parent/child node IDs
			if ($verbosity ==1) {
				print "parent of node $node : $parent \n";
			}

			if (exists($minimum_bounds{$parent})) { # begin loop 3
			
				if (exists($outgroup_FADS{$node})) { # begin loop 3b
			
					if ($minimum_bounds{$parent} > max(@{$outgroup_FADS{$node}})) { # begin loop 3c
			
						push @{$outgroup_FADS{$node}}, $minimum_bounds{$parent};			
					
						# keep track the minimum bounds of parent nodes
						if ($verbosity == 1) {
							print "node $node has parent $parent with minimum constraint : $minimum_bounds{$parent}\n";
							print "maxmimum is greater than the oldest FAD\n";
						} 
					} # end loop 3c
					else {
						if ($verbosity==1) {
							print "node $node has ancestor $parent with minimum constraint : $minimum_bounds{$parent}\n";
							print "this maxmimum is not greater than the oldest FAD\n";
							print "forget it and move onto the next parent node\n";
						}
					}
				} # end loop 3b
			
				else { # begin loop 3d
					push @{$outgroup_FADS{$node}}, $minimum_bounds{$parent};
				} # end loop 3d
			
			} # end loop 3
			
			else {
				if ($verbosity==1) {
					print "node $node has parent $parent with no minimum constraint";
				}
			}		

			foreach my $i (1..$max_nodes) { # begin loop 5
		 		
			 	if ($verbosity == 1) {
		 			print '$i : ', "$i\n";
			 	}		 			
		 	
			 	if (exists($parent{$parent})) { # begin loop 7
			 	# if the parent node has a parent 		
		 			
		 			$parent = $parent{$parent}; # define the current parent's parent node as the new current parent
		 				
		 			if ($verbosity == 1) {
						print "parent of previous parent : $parent\n";
				 	}
	
					if (exists($minimum_bounds{$parent})) { # begin loop 8
					# if that parent has a minimum bound										
							
						if ($verbosity == 1) {
							print "Found a minimum!!\n";
						}
						
						# if outgroup FADs already exist
						if (exists($outgroup_FADS{$node})) { # begin loop 8b
							if ($minimum_bounds{$parent} > max(@{$outgroup_FADS{$node}})) { # begin loop 8c
					
								push @{$outgroup_FADS{$node}}, $minimum_bounds{$parent};
	
								if ($verbosity == 1) {
									print "node $node has ancestor $parent with minimum constraint : $minimum_bounds{$parent}\n";
									print "maxmimum is greater than the oldest FAD\n";
								}
							} # end loop 8c
							else {
								if ($verbosity==1) {
									print "node $node has ancestor $parent with minimum constraint : $minimum_bounds{$parent}\n";
									print "this maxmimum is not greater than the oldest FAD\n";
									print "forget it and move onto the next parent node\n";
								}
							}							
						
						} # end loop 8b
					
						# otherwise store the FAD
						else { # begin loop 8d
						
							if ($verbosity==1) {
								print "no, outgroup FADs don't exist\n";
							}
						
							push @{$outgroup_FADS{$node}}, $minimum_bounds{$parent};
						} # end loop 8d
					
					} # end loop 8
				
				} # end loop 7
			} # end loop 5
		} # end loop 2			
	} # end loop 1b
	
	else {
		if ($verbosity==1) {
			print "no minimum bounds exist for this node\n";
		}
	}		
	
	if ($verbosity==1) {
		print "\n";
	}
	
} # end loop 1

# ----
return(\%minimum_bounds,\%outgroup_FADS);

# NB subroutine max resides in FirstAppearances.pm

}

1;
