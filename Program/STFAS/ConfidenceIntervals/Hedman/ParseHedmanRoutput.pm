#!/usr/bin/per;
use strict;
use warnings;

# this function parses the output from analysis (Hedman, 2010) in R
# and returns a hash containing the maximum bound based on the 97.5% posterior probability

sub ParseHedmanRoutput {

my $i=$_[0];
my %options=%$i;

# required input
my $resolution=$options{r};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
# open the R output 
open (OUTPUT, "Hedman_output.txt") || die "can't open Hedman output!\n";

my @lines = <OUTPUT>;

# ----
# organise the output

# seperate the data for each node
my @headers;

foreach my $lines(@lines) {
	# if lines contain any non-digit 3+ times
	if ($lines =~ /\D\D\D+/ ) {
		
		# remove newline characters
		chomp $lines;
		
		# split coloums by any whitespace characters
		@headers=split(/\s+/, $lines);
	}
}

# raname the columns to match the node labels
foreach my $header (@headers) {
	$header =~ s/age_//g;
	$header =~ s/_/:/g;	
}

# assign the posteriors to each node
my @values;
my $total_headers=scalar(@headers);
my %results;
my @time;

foreach my $lines (@lines) {
	
	# unless line begins any non-digit 3+ times
	unless ($lines =~ /^\D\D\D+/) {
		
		# split each line by any whitespace characters
		@values=split(/\s+/, $lines);

		# the first element of the array
		shift(@values);
		
		# record the time at each step 
		push @time, $values[0];
		
		# place the posterior distribution for each node in an individual array
		foreach my $values (1..$total_headers-1) {
			my $current_headers=$headers[$values];
			push @{$results{$current_headers}}, $values[$values];
		}
	}
}

close OUTPUT;

# ----
# specify the number of decimal places to use in the output
# using the same strategy used by the functions SimUnifOccurrences & SimNonUnifOccurrences
my $sig_figs=length($resolution);
my $sprintf="%.$sig_figs"."f";

# ----
# calculate the posterior intervals
my %hedman_max;
my %hedman_lower;
my %hedman_mid;
my $pp=0; # posterior probability
my $step_no=-1;
my $positive_age;
foreach my $node (keys %results) {
	
	if ($verbosity==1) {
		print "Looking at node $node\n";
	}

	# mid posterior interval
#	foreach my $probability (@{$results{$node}}) {
#		unless ($pp >= 0.5) {
#			$pp=$pp+$probability;
#			$step_no=$step_no+1;
#		}
#	}
#	$positive_age=$time[$step_no]*-1;
#	$hedman_mid{$node}=$positive_age;
#	$pp=0;
#	$step_no=0;

	# lower posterior interval
#	foreach my $probability (@{$results{$node}}) {
#		unless ($pp >= 0.025) {
#			$pp=$pp+$probability;
#			$step_no=$step_no+1;
#		}
#	}
#	$positive_age=$time[$step_no]*-1;
#	$hedman_lower{$node}=$positive_age;
#	$pp=0;
#	$step_no=0;

	# upper posterior interval
	
	# run through the posterior values
	foreach my $probability (@{$results{$node}}) {
		
		# unless the posterior is greater than .975
		unless ($pp >= 0.975) {
			
			# tot up the the probability
			$pp=$pp+$probability;
			# and move onto the next step
			$step_no=$step_no+1;
		}
	}
	
	# get the time step for which the posterior probability was < 0.975 by one step
	$step_no=$step_no-1;
	
	# get the true time at that time step
	$positive_age=$time[$step_no]*-1;
	
	# run the output age
	my $positive_age_round = sprintf "$sprintf", $positive_age;
	
	# store the maximum bounds
	$hedman_max{$node}=$positive_age_round;
	
	if ($verbosity==1) {
		print "step for p ~ 0.975 is $step_no\n";
		my $R_step=$step_no+1;
		print "this is equivalent to step $R_step in R\n";
		print "age at this time step is $positive_age_round\n\n";
	}
	
	$pp=0;
	$step_no=-1;

}

# normally
return(\%hedman_max);

# for testing
#return(\%hedman_mid,\%hedman_lower,\%hedman_max);


}

1;
