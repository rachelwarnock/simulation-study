#!/usr/bin/perl
use strict;
use warnings;
use ReadTree;
use ConfidenceIntervals::ReadOccurrences;

# this functions reads the stratigraphic occurrence and calculates the first appearance for each node 
# and returns a hash containing the information
# the value returned is the "oldest secure" age of the fossil = the minimum age of the horizon in which the fossil was found
# The occurence data is read for individual branches (labelled according to their descendent terminals) but note the first appearances are output for individual nodes (also labelled according to their descendent terminals)

sub FirstAppearances {

my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{y};
my $resolution=$options{r};
my $max_age=$options{a};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
#ReadTree - subroutine written by Julian Gough
my @temp=ReadTree($treefile,$verbosity); # subroutine returns (\%nodeup,\%distances,\%nodedown);

$i=$temp[2]; # third parameter is stored in $temp[2]
my %childs=%$i; # place %nodedown in hash %childs

# ----
#ReadOccurrences
@temp=ReadOccurrences(\%options);

$i=$temp[0];

my %occurrences=%$i; # place %occurrences in hash %occurrences

# ----
# work out the length of each horizon
# using the same rounding strategy used buy the functions SimUnifOccurrences & SimNonUnifOccurrences
my $sig_figs=length($resolution);
my $sprintf="%.$sig_figs"."f";

my %horizon_length;
my $horizon_length;
my $current_horizon;
my $previous_horizon=0;

foreach my $horizon (1..$resolution) {

	# work the horizon ID (equivalent to the maximum horizon age)
	$current_horizon=($horizon/$resolution) * $max_age;

	# round to the nearest decimal place (specified above)
	$current_horizon=sprintf("$sprintf", $current_horizon);

	# work out the time length of the horizon
	$horizon_length=$current_horizon-$previous_horizon;

	# store the the horizon length
	$horizon_length{$current_horizon}=$horizon_length;
	
	$previous_horizon=$current_horizon;
	
}

# ----
# Defining minima based on first occurrences

my %daughter_occurrences; # hash for occurrence data used to define minimum constraints
my %minimum_bounds; # has for minimum bounds based on first occurrences

foreach my $node (keys %childs) {
	# for each node
	
	my @lineages = split(',', $childs{$node});
	# split the the lineages by ','
	
	foreach my $lineage(@lineages) {
		
		if ($verbosity ==1) {
			print "for node $node the lineage $lineage exists \n";
		}
		
		if (exists($occurrences{$lineage})) {
			# if occurrence data exists for lineage
						
			if ($verbosity ==1) {
				print "Occurrences for lineage $lineage : @{$occurrences{$lineage}}\n";
			}
			
			push @{$daughter_occurrences{$node}}, @{$occurrences{$lineage}};
			# add occurrence data to hash
	
			if ($verbosity ==1) {
				print "Occurrences for $node : @{$daughter_occurrences{$node}}\n";
			}
			
		}
		
		else {
			if ($verbosity ==1) {
				print "there are no fossils for lineage $lineage\n";
			}
		}
	}
			
	if (exists($daughter_occurrences{$node})) {
		# if occurrence data exists
		
		my $oldest_occurrence = &max(@{$daughter_occurrences{$node}});
		
		$horizon_length=$horizon_length{$oldest_occurrence};

		my $oldest_secure=$oldest_occurrence-$horizon_length;
		# define the minima
		#  N.B must be oldest secure, substract 0.1 (or alternative correct resolution) for the youngest age of the horizon
		
		if (($oldest_secure == 0) || ($oldest_secure < 0.0001)) {
			$oldest_secure=0.0001;
		} # this is required because mcmctree can not handle zero minimum bounds
		
		$minimum_bounds{$node}=$oldest_secure;

		if ($verbosity ==1) {
			print "oldest occurrence for $node = $oldest_occurrence \n";
			print "oldest secure for $node = $oldest_secure \n";
			print "minima for $node = $minimum_bounds{$node} \n\n";
		}
		
	}
	else {
		if ($verbosity ==1) {
			print "there are no fossils for node $node\n\n";
		}
	}

}

return(\%minimum_bounds); # backslash creates a reference to the hash

}

# this sub routine finds the maximum number in an array
# and returns the largest value
sub max {
	my ($max_so_far) = shift @_;
	foreach (@_) {
		if ($_ > $max_so_far) {
			$max_so_far = $_;
		}
	}
	$max_so_far;
}

1;

