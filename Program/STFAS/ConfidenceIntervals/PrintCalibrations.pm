#!/usr/bin/perl
use strict;
use warnings;
use ReadTree;

# this function prints out a file containing the minimum and maximum constraints for each node
# this file is useful for later analysis
# if a node has no minimum bound it prints out "0"
# if a node has no maximum bound it prints out "Inf"
# this function does not handel the case where a node has a maximum but no minimum

sub PrintCalibrations {

my $i=$_[0];
my %options=%$i;

$i=$_[1];
my %minimum_bounds=%$i;
$i=$_[2];
my %maximum_bounds=%$i;

# required input
my $treefile=$options{y};
my $upperbound=$options{u};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
#ReadTree - subroutine written by Julian Gough
my @temp=ReadTree($treefile,$verbosity); # subroutine returns (\%nodeup,\%distances,\%nodedown);

$i=$temp[0]; # first parameter is stored in $temp[0] 
my %parent=%$i; # place %nodeup in hash %parent

$i=$temp[2]; # third parameter is stored in $temp[2]
my %childs=%$i; # place %nodedown in hash %childs

# ----
open (CALS, ">constraints.txt") || die "can't open constraints file\n";

# for each node
foreach my $node (keys %childs) { # begin loop 1
	
	if ($verbosity==1) {
		print "Looking at node $node\n";
	}

	# if this node has a parent node (e.g. it is not the root)
	if (exists($parent{$node})) { # begin loop 2
	
	if ($verbosity==1) {
		print "Node $node is not the root\n";
	}
	
		# if the node has a maximum bound
		if (exists $maximum_bounds{$node}) { # begin loop 3
		
			if ($verbosity==1) {
				print "yes node $node has a maximum\n";
			}
			
			# if the node also has a minimum bound
			if (exists $minimum_bounds{$node}) { # begin loop 4
			
				if ($verbosity==1) {
					print "yes node $node also has a minumum\n";
				}
				
				# print the bounds
				print CALS "$node: $minimum_bounds{$node} $maximum_bounds{$node}\n";
				
			} # end loop 4
		} # end loop 3
		
		# otherwise if node just has a minimum constraint
		elsif (exists $minimum_bounds{$node}) { # begin loop 5
			
			if ($verbosity==1) {
				print "node $node has no maximum…\n";
				print "…but node $node has a minimum!\n";
			}
			
			# print the bounds
			print CALS "$node: $minimum_bounds{$node} Inf\n";
		} # end loop 5
		
		else { # begin loop 6
			
			if ($verbosity==1) {
				print "node $node has no calibrations\n";
			}
			
			# print the bounds
			print CALS "$node: 0 Inf\n";
			
		} # end loop 6
	} # end loop 2
	
	# if the node is the root
	else { # begin loop 7
		
		if ($verbosity==1) {
			print "node $node is the root!\n";
		}
		
		# if root has a maximum…
		if (exists $maximum_bounds{$node}) { # begin loop 8
			
			# and a minimum...
			if (exists $minimum_bounds{$node}) { # begin loop 9
			
				if ($verbosity==1) {
					print "the root has min and max bounds\n";
				}
				
				# print bounds
				print CALS "$node: $minimum_bounds{$node} $maximum_bounds{$node}\n";
				
			} # end loop 9
		} # end loop 8
		
		# otherwise if the root has a minimum bound only
		elsif (exists $minimum_bounds{$node}) { # begin loop 10
		
			if ($verbosity==1) {
				print "the root has no maximum…\n";
				print "…but the root has a minimum!\n";
			}
			
			# print bounds
			print CALS "$node: $minimum_bounds{$node} $upperbound\n";
		} # end loop 10
		
		# if the root has bounds
		else { # begin loop 11
		
			if ($verbosity==1) {
				print "the root has no min & no max\n";
			}
			
			# print bounds
			print CALS "$node: 0 $upperbound\n";
		} # end loop 11
	} # end loop 7
	
	if ($verbosity==1) {
		print "\n";
	}

} # end loop 1

close CALS;
	
}

1;