#!/usr/bin/perl
use strict;
use warnings;

sub SkewPr {
	
my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{y};
my $filepath=$options{P};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

#---------
# create a label for all output files
my ($current_tree_abbrev, $txt) = split(/\./, $treefile);

#---------
# define the location of the source
my $source_dir=$filepath."Program/STFAS/ConfidenceIntervals/SkewPr/";
#my $library_dir=$filepath."Program/R_libs";
#---------
# print the R commands

open (SKEW, ">Skew_commands.R") || die "can't create Skew Pr commands file!";
print SKEW "library(ape)
#library(sn)
source.dir<-\"$source_dir\"
source(file.path(source.dir,\"print.mcmctree.R\"))
source(file.path(source.dir,\"mcmctree.labels.R\"))
source(file.path(source.dir,\"sn.R\"))
source(file.path(source.dir,\"st.R\"))
tree<-read.tree(\"R.$current_tree_abbrev.txt\")
tree\$node.label<-mcmctree.labels(tree\$node.label)
print.mcmctree(tree,file=\"mcmctree.$current_tree_abbrev.txt\")\n";
close SKEW;

#---------
# run analysis in R
system ("R --vanilla --slave --no-save < Skew_commands.R");	
#---------

}

1;
