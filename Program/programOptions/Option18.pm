#!/usr/bin/perl
use strict;
use warnings;
use ResultsTable;

# Option18 generate an output file for plotting the mean and median in ggplot
# note this only produces output for posterior output files

sub Option18 {

my $i=$_[0];
my %options=%$i;

my $trees=$options{t};
my $tree_counter=$options{F};
my $stop=($tree_counter+$trees)-1;

my $filename="Results.median.txt";

open (EVERYTHING, ">Results.median.txt") || die "Can't open file 'Everything' \n";

foreach my $tree ($tree_counter..$stop) {
	my $dir="tree$tree_counter";
	chdir $dir;
	$options{y}="tree$tree_counter.txt";
	ResultsTable(\%options);
	$dir="..";
	chdir $dir;
	$tree_counter=$tree_counter+1;
}

close EVERYTHING;

my $logID=$options{l};
system("mv $filename $logID.$filename");

print "Analysis of output not automated!\n";

}

1;