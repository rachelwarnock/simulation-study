#!/usr/bin/perl
use strict;
use warnings;
use PrintOptions;
use PrintSwitches;

# this function reads all input parameter options and assigns default values to all unsepcified values
# the function has some limited capacity for finding incorrectly specified parameters

sub ProgramOptions {

my $i=$_[0];
my %options=%$i;

# ----

PrintSwitches() if defined $options{h};

# what do you want to do with the program?
if (!$options{f}) {
	die "Specify a program option using -f!\nUse -h to view all program options.\n";
}

my @fvals=(1,2,3,4,5,6,7,8,18);
unless ( grep( /^$options{f}$/, @fvals ) ) {
  die "Specify a valid program option using -f 1 to 8, 17 or 18!\n";
}

# ----
# scale of the simulations
# number of trees 
# I decided that it was useful to always specify tree number
if (!$options{t}) {
	die "Specify the number trees you've got using -t!\n";
}
# tree start
$options{F}=1 unless defined $options{F};

PostiveInteger($options{t},"number of trees -t") if defined $options{t};

# number of taxa
if ($options{n}) {
	PostiveInteger($options{n},"number of taxa -n") if defined $options{n};
	if ($options{n} < 3) { die "number of taxa -n must be greater than 2!\n" }
}
# ----

# tree file
$options{i}="simulated_trees.nex" unless defined $options{i};
FileNames($options{i},"filenames -i") if defined $options{i};

# ----

$options{a}=1 unless defined $options{a};
PositiveReal($options{a},"age -a") if defined $options{a};

# parameters for simulating trees (option 8); I don't think this will continue to be useful: consider deleting
# apart from anything else it doesn't work!
if ($options{f} == 8) {
	
	# number of taxa
	if (!$options{n}) { die "Specify the number of taxa using -n!\n" }	
	
	# extinction rate
	PositiveReal($options{e},"extinction rate -e") if defined $options{e};
	$options{e}=0.1 unless defined $options{e};
		
	# birth rate
	PositiveRealNotZero($options{g},"speciation rate -g") if defined $options{g};
	$options{g}=1 unless defined $options{g};
	
	if ($options{g} <= $options{e}) {
		die "Origination rate (-g) must be greater than extinction rate (-e)!\n"
	} 
	
}

# ----
# parameters for simulating occurrence data, or generating confidence intervals (options 2,3 or 5)

if ($options{f} =~ m/^[2|3|5|9]$/) {
	# resolution
	if (!$options{r}) { die "Specify the resolution using -r!\n" }
	PostiveInteger($options{r},"resolution -r (number of time bins)");
	if ($options{r} < 2 ) { 
		die "The resolution -r (number of time bins) must be greater than 1!\n"
	}
}

# parameters for simulating uniform occurrence data (option 2)
if ($options{f}==2) {
	if (!$options{s}) { die "Specify the sampling intensity using -s!\n" }
	PositiveRealOne($options{s},"sampling intensity -s");
}

# parameters for simulating non-uniform occurrence data (option 3)
if ($options{f}==3) {
	
	if (!$options{A}) { die "Specify the peak abundance using -A!\n" }	
	if (!$options{D}) { die "Specify the preferred depth using -D!\n" }
	if (!$options{T}) { die "Specify the depth tolerance using -T!\n" }
	
	PositiveReal($options{A},"Peak abundance -A") if defined $options{A};
	PositiveReal($options{D},"Preferred depth -D") if defined $options{D};
	PositiveReal($options{T},"Depth tolerance -T") if defined $options{T};
}
# maximum water depth
$options{w}=2 unless defined $options{w};
PositiveReal($options{w},"Maximum water depth -w") if defined $options{w};
# ----

# parameters for simulating sequence data
if ($options{f}==4) {	
	$options{C}=1000 unless defined $options{C};
	PostiveInteger($options{C},"alignment length -C");
	if (!$options{n}) { die "Specify the number of taxa using -n!\n" }	
	$options{z}=0.01 unless defined $options{z}; # this should be zero
	PositiveReal($options{z},"variance -z"); # is that true?
	$options{W}=1 unless defined $options{W};
}

# parameters for simulating sequence data + the analysis of sequence data
if ($options{f} =~ m/^[4|6|7]$/) {
	$options{L}=1 unless defined $options{L};
	$options{S}=1 unless defined $options{S};
	PostiveInteger($options{S},"sequence replicates -S");
	$options{M}=4 unless defined $options{M};
	$options{z}=0.01 unless defined $options{z};
	unless ($options{M} =~ m/^[0|4]$/) {
		die "Options for nucleotide substitution model are 0 (JC69) or 4 (HKY85) using -M!\n";
	}
}
# ----
# parameters for parsing data
if ($options{f}==9) {
	unless(-e $options{i}) { die "Specify the real tree file using -i!\n" }	
}

# ----
# confidence intervals
if ($options{f}==5) {
	
	# parameters for generating confidence intervals
	$options{k}='M' unless defined $options{k};
	if ($options{k} !~ m/^[B|b|L|l|M|m|H|h|a|A]$/) { die "Generate constraints -k using B, L, M, C, H, S or A \n" }
	$options{j}='U' unless defined $options{j};
	if ($options{j} !~ m/^[N|n|U|u]$/) { die "Options for calibration priors are non-uniform (n) or uniform (u) using -j\n" }
	# statement required about possible values of U
	$options{U}=1.5 unless defined $options{U};
	
	# prior parameters
	# for minimum constraints in mcmctree (the truncated Cauchy distributon)
	$options{p}=0.1 unless defined $options{p}; # what can these be?
	$options{c}=1 unless defined $options{c};
	$options{m}=0.01 unless defined $options{m};
	#PositiveRealProb($options{m},"soft minimum -m") if defined $options{m};

	# maximum constraints in mcmctree
	$options{b}=0.025 unless defined $options{b};
	#PositiveRealProb($options{b},"soft maximum -b") if defined $options{b};

}

if ($options{f} =~ m/^[5|6|7]$/) {
	# upper bound
	if (!$options{u}) { die "Specify the upper bound -u!\n" }
	PositiveReal($options{u},"upper bound -u") if defined $options{u};
}

# ----
# mcmc settings

$options{x}=40000 unless defined $options{x};
$options{X}=40000 unless defined $options{X};

PositiveReal($options{x},"chain length -x") if defined $options{x};
PositiveReal($options{X},"chain length -X") if defined $options{X};

if ($options{x} < 10000) {
	die "Chain length -x (posterior) must be at least 10000 (10 * 10 ^ 3) steps\n";
}
if ($options{X} < 10000) {
	die "Chain length -X (prior) must be at least 10000 (10 * 10 ^ 3) steps\n";
}

my $sample_freq;
if (defined $options{x}) { 
	$sample_freq=$options{x}/20000;
}
elsif (defined $options{X}) { 
	$sample_freq=$options{X}/20000;
}
if (defined $sample_freq) { 	
	unless ($sample_freq =~ /^\+?\d+$/) {		
		die "chain length must be a positive integer divisible by 20000\n";
	}
}

# ----
if ($options{f} == 18) {
	if (!$options{l}) {
		die "What is the log file id? I'm scared you're going to make a mess. -l\n"
	}
	if (!$options{L}) {
		die "Specify the number of loci. -L\n"
	}
}
else {	
	# log file ID & random seed
	$options{l}=$options{B} unless defined $options{l};
}
# ----

# verbosity & debugging options
$options{v}=1 if defined $options{v};
$options{d}=1 if defined $options{d};


# print out program options to file
PrintOptions(\%options);
# ----
return(\%options);
	
}

# ----

sub PostiveInteger {
	unless ($_[0] =~ /^\+?\d+$/) {
		die "$_[1] must be a positive integer number\n";
	}
}

sub PositiveReal {
	unless ($_[0] =~ /^\+?\d+\.?\d*$/) { # regex also matches zero
		die "$_[1] must be a positive real number!\n";
	}
}

#sub PositiveRealOrZero {
#	unless ($_[0] == 0) {
#		unless ($_[0] =~ /^\+?\d+\.?\d*$/) {
#			die "$_[1] must be a positive real number!\n";
#		}
#	}
#}

sub PositiveRealNotZero {
	unless ($_[0] =~ /^\+?\d+\.?\d*$/) {
		die "$_[1] must be a positive real number!\n";
	}
	unless ($_[0] != 0) {
		die "$_[1] can not be zero!\n";
	}
}

sub PositiveRealProb {
	unless ($_[0] =~ /^\+?\d+\.?\d*$/) {
		die "$_[1] must be a positive real number less than one!\n";
	}
	unless ($_[0] < 1) {
		die "$_[1] must be a positive real number less than one!\n";
	}
}

sub PositiveRealOne {
	unless ($_[0] =~ /^\+?\d+\.?\d*$/) {
		die "$_[1] must be a positive real number less than or equal to one!\n";
	}
	unless ($_[0] <= 1) {
		die "$_[1] must be a positive real number less than or equal to one!\n";
	}
}

sub FileNames {
	if ($_[0] =~ /^\./) {
		unless ($_[0] =~ /^\.\./) {
			die "Filenames can not begin with \".\"\n";
			# mac os x file names can not begin with '.'
		}
	}
	if ($_[0] =~ /[\*:|"<>?\\]/) {
		die "Filenames can not contain the following symbols \"?<>\\:*|\"\"\n";
		# invalid file or folder name characters on windows / ? < > \ : * | "
		# I have made an exception here for '/' because this could refer to 
		# a filepath. 
	}
}

# there is some limited flexibility interpreting floating point numbers

1;

