#!/usr/bin/perl
use strict;
use warnings;
use ParseNexusMPISeqRep;

# Option17 parse simulated trees file for MPI run

sub Option17 {

my $i=$_[0];
my %options=%$i;

ParseNexusMPISeqRep(\%options);
	
}

1;