#!/usr/bin/perl
use strict;
use warnings;
use SimUnifOccurrences;

# Option2 simulates uniform occurrence data for existing trees (in exisiting sub folders)

sub Option2 {

my $i=$_[0];
my %options=%$i;

my $trees=$options{t};
my $tree_counter=$options{F}; # start tree 
my $stop=($tree_counter+$trees)-1; # stop tree

foreach my $tree ($tree_counter..$stop) {
	my $dir="tree$tree_counter";
	chdir $dir;
	$options{y}="tree$tree_counter.txt";
	SimUnifOccurrences(\%options);
	$dir="..";
	chdir $dir;
	$tree_counter=$tree_counter+1;
}
	
}

1;