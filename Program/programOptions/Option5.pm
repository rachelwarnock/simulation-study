#!/usr/bin/perl
use strict;
use warnings;
use ConfidenceIntervals::Constraints;
use ResultsTable;

# Option5 generates confidence intervals

sub Option5 {

my $i=$_[0];
my %options=%$i;

my $trees=$options{t};
my $tree_counter=$options{F};
my $stop=($tree_counter+$trees)-1;

my $filename="Results.txt";

open (EVERYTHING, ">$filename") || die "Can't open file 'Everything' \n";

foreach my $tree ($tree_counter..$stop) {
	my $dir="tree$tree_counter";
	chdir $dir;
	$options{y}="tree$tree_counter.txt";
	Constraints(\%options);
	ResultsTable(\%options);
	$dir="..";
	chdir $dir;
	$tree_counter=$tree_counter+1;
}

close EVERYTHING;

my $logID=$options{l};
system("mv $filename $logID.$filename");


}

1;