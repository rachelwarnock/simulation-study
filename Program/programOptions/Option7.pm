#!/usr/bin/perl
use strict;
use warnings;
use PosteriorMCMCTREE;
use ResultsTable;

# Option7 posterior analysis using mcmctree

sub Option7 {

my $i=$_[0];
my %options=%$i;

my $trees=$options{t};
my $tree_counter=$options{F};
my $stop=($tree_counter+$trees)-1;

my $filename="Results.txt";

open (EVERYTHING, ">Results.txt") || die "Can't open file 'Everything' \n";

foreach my $tree ($tree_counter..$stop) {
	my $dir="tree$tree_counter";
	chdir $dir;
	$options{y}="tree$tree_counter.txt";
	PosteriorMCMCTREE(\%options);
	ResultsTable(\%options);
	$dir="..";
	chdir $dir;
	$tree_counter=$tree_counter+1;
}

close EVERYTHING;

my $logID=$options{l};
system("mv $filename $logID.$filename");

}

1;