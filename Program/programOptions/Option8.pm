#!/usr/bin/perl
use strict;
use warnings;
use SimulateTrees;

# Option8 simulates trees

sub Option8 {

my $i=$_[0];
my %options=%$i;

SimulateTrees(\%options);
	
}

1;