#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# ProgramOptions.pl
# test function: ProgramOptions
# package: ProgramOptions
# other functions required: PrintOptions

my %options=();

# all program options
getopts("a:A:b:Bc:C:e:f:g:i:j:k:dD:m:n:p:r:Rs:S:t:T:u:vw:X:x:y:z:", \%options);

# use $options{v} or $options{d} to switch on verbose mode

ProgramOptions(\%options);

# ----
# specify file paths

my $current_dir; # test script directory
my $ProgramOptions; # ProgramOptions functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$ProgramOptions=$_;
}

BEGIN {

	use lib $ProgramOptions;
	use ProgramOptions;

}

# -a : age at the root/age of the basin
# -A : peak abundance
# -b : maximum bound tail
# -B : switch on analysis in BEAST
# -c : c-value
# -C : alignment length
# -d : switch on debugging mode
# -D : depth tolerance
# -e : extinction rate
# -f : program options
# -g : birth rate
# -i : simulated trees file
# -j : prior distributions
# -k : constraints
# -m : minimum bound tail
# -M : substitution model
# -n : number of taxa
# -p : p-value
# -r : resolution = number of horizons
# -s : sampling intensity
# -S : no. of sequence replicates
# -t : number of trees
# -T : depth tolerance
# -u : upper bound
# -v : verbost mode
# -w : maximum water depth
# -x : MCMC steps
# -y : treefile
# -z : variance

