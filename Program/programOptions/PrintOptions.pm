#!/usr/bin/perl

# this function prints out all program options & swithces

sub PrintOptions {

my $i=$_[0];
my %options=%$i;

my $filename="Program_Options.txt";

open my $fh, '>', $filename or die "can't open file";

print $fh "\nThe program was called using the following options:\n";

# ----
# what did you ask the program to do?
print $fh "Program option -f = $options{f} : the program was asked to ";

if ($options{f} == 1) {
	print $fh "parse an existing nexus treefile\n";
}
if ($options{f} == 2) {
	print $fh "simulate uniform occurrences\n";
}
if ($options{f} == 3) {
	print $fh "simulate non-uniform occurrences\n";
}
if ($options{f} == 4) {
	print $fh "simulate sequences\n";
}
if ($options{f} == 5) {
	print $fh "generate confidence intervals\n";
}
if ($options{f} == 6) {
	print $fh "prior molecular clock analysis\n";
}
if ($options{f} == 7) {
	print $fh "posterior molecular clock analysis\n";
}
if ($options{f} == 8) {
	print $fh "simulate trees\n";
}
if ($options{f} == 17) {
	print $fh "parse exisiting trees for MPI run\n";
}
if ($options{f} == 18) {
	print $fh "generate a results file including the median\n";
}

print $fh "----\n";

# ----
# scale of simulations
print $fh "Scale of the simulations and analyses\n";

# trees
print $fh "Number of trees -t = $options{t}\n";
print $fh "Tree start number -F = $options{F}\n";

# taxa
if ($options{n}) { print $fh "Number of taxa -n = $options{n}\n" }

print $fh "----\n";

if ($options{f} == 1) {
	print $fh "Trees used for simulating data should be found in file -i = $options{i}\n";
	print $fh "----\n";
}

# ----

# MPI specs

if ($options{f} == 17) {
	print $fh "Number of sequence replicates required -S* = $options{S}\n*Note this is not a conventional use of this parameter\n";
}

# ----
# begin simulated data settings
if ($options{f} =~ m/^[2|3|4|8]$/) {

	# ---- Temporal parameters	
	# parameters used to simulate occurence data/generate confidence intervals
	if ($options{f} =~ m/^[2|3]$/) {
		print $fh "Temporal resolution and parameters\n"; 	
		# root age/maximum basin age
		if ($options{a} == 1) {
			print $fh "Maximum age of the basin -a = $options{a} (default)\n";
		}
		else {
			print $fh "Maximum age of the basin -a = $options{a}\n";
		}
		print $fh "Resolution (number of time bins) -r = $options{r}\n";
		print $fh "----\n";
	}	

	if ($options{f}==8) {
		# parameters used to simulate trees
		print $fh "Parameters used to simulate trees using TreeSim\n";		
	
		# extinction rate
		if ($options{e} == 0.1) {
			print $fh "Extinction rate -e = $options{e} (default)\n";
		}
		else {
			print $fh "Extinction rate -e = $options{e}\n";
		}
		# birth rate
		if ($options{g} == 1) {
			print $fh "Birth rate -g = $options{g} (default)\n";
		}
		else {
			print $fh "Birth rate -g = $options{g}\n";
		}
		
		# trees file
		print $fh "Simulated trees printed to the file $options{i}\n";	
		print $fh "----\n";
	}	

	# uniform sampling
	if ($options{f}==2) {
		print $fh "Simulating fossil data\n";
		print $fh "Parameters used to simulate uniform occurrence data\n";		
		print $fh "Sampling intensity -s = $options{s}\n";
		print $fh "----\n";
	}

	# non-uniform sampling
	if ($options{f}==3) {
		print $fh "Simulating fossil data\n";
		print $fh "Parameters used to simulate non-uniform occurrence data (Holland, 1995)\n";
		print $fh "Peak abundance -A = $options{A}\n";
		print $fh "Preferred depth -D = $options{D}\n";
		print $fh "Depth tolerance -T = $options{T}\n";
		if ($options{w} == 2) {
			print $fh "Maximum water depth = $options{w} (default)\n";
		}
		else {
			print $fh "Maximum water depth = $options{w}\n";
		}
		print $fh "Number of transgression/regression events = 2 (hard coded)\n";
		print $fh "----\n";
	}

	# ----
	# parameters used to simulate sequence data
	if ($options{f}==4) {
		print $fh "Parameters used to simulate sequence data\n";
		if ($options{C} == 1000) { print $fh "Alignment length -C = $options{C} (default)\n" }
		else { print $fh "Alignment length -C = $options{C}\n" }
		if ($options{S} == 1) { print $fh "Number of sequence replicates -S = $options{S} (default)\n" }
		else { print $fh "Number of sequence replicates -S = $options{S}\n" }
		if ($options{L} == 1) { print $fh "Number of loci -L = $options{L} (default)\n" }
		else { print $fh "Number of loci -L = $options{L}\n" }
		if ($options{z} == 0) { print $fh "Sequences simulated using the strict clock model\nVariance -z = $options{z}\n" } # this should really be the default
		elsif ($options{z} == 0.01) { print $fh "Sequences simulated using a relaxed clock model\nVariance -z = $options{z} (default)\n" }
		else { print $fh "Sequences simulated using a relaxed clock model\nVariance -z = $options{z}\n" }
		if ($options{W} == 1) { print $fh "Mean of the gamma distribution of rates for independent loci -W = $options{W} (hard coded)\n" }
		else { print $fh "Mean of the gamma distribution of rates -W = $options{W}\n" }		
		if ($options{M} == 0) { print $fh "Model of sequence evolution -M = 0 (JC69)\n" }
		elsif ($options{M} == 4) { print $fh "Model of sequence evolution -M = 4 (HKY85) (default)\n" }		
		print $fh "----\n";
	}

} # simulated data settings 

# ----

if ($options{f}==5) { # begin settings for analysis of simualted data

	# ----
	# parameters used for generating constraints
	print $fh "Generating constraints\n";

	# fossil ages
	print $fh "Parameters used to date the fossils\n";
	if ($options{a}==1.2) {
		print $fh "Age of the basin -a = $options{a} (default)\n";
	}
	else {
		print $fh "Age of the basin -a = $options{a}\n";
	}
	print $fh "Resolution (number of time bins) -r = $options{r}\n";
	print $fh "----\n";

	# type of constraints
	if ($options{k} =~ m/^[L|l]$/) {
		print $fh "Type of constraints -k = Lower bounds only\n";
	}
	if ($options{k} =~ m/^[M|m]$/) {
		print $fh "Type of constraints -k = Marshall (2008) (default)\n";
	}
	if ($options{k} =~ m/^[C|c]$/) {
		print $fh "Type of constraints -k = Marshall (conservative) (2008)\n";
	}
	if ($options{k} =~ m/^[B|b]$/) {
		print $fh "Type of constraints -k = Best practice\n";
	}
	if ($options{k} =~ m/^[H|h]$/) {
		print $fh "Type of constraints -k = Hedman (2010)\n";
	}
	if ($options{k} =~ m/^[A|a]$/) {
		print $fh "Type of constraints -k = arbitrary SN\n";
		print $fh "Max -U = $options{U} of the upper bound\n";
	}	
	print $fh "----\n";
	print $fh "Calibration priors\n";
	
	# type of priors
	if ($options{j} =~ m/^[U|u]$/) {
		print $fh "Priors distributions -j = uniform (default)\n";
	}	
	if ($options{j} =~ m/^[N|n]$/) {
		print $fh "Prior distributions -j = non-uniform\n";
	}
	
	# upper limit
	print $fh "Upper bound (multiple of the rootage) -u = $options{u}\n";
	print $fh "----\n";
	
	# mcmctree settings
	print $fh "MCMCTREE options\n";
	# minima
	if ($options{p} == 0.1) { print $fh "p-value -p = $options{p} (truncated Cauchy distribution) (default)\n" }
	else { print $fh "p-value -p = $options{p} (truncated Cauchy distribution)\n" }
	if ($options{c} == 1) { print $fh "c-value -c = $options{c} (truncated Cauchy distribution) (default)\n" }
	else { print $fh "c-value -c = $options{c} (truncated Cauchy distribution)\n" }
	if ($options{m} eq '1e-300') { print $fh "Minimum bound -m = $options{m} (hard) (default)\n" }
	else { print $fh "Minimum bound -m = $options{m}\n" }
	# maxima
	if ($options{b} eq '1e-300') { print $fh "Maximum bound tail -b = $options{b} (hard) (default)\n" }
	else { print $fh "Maximum bound tail -b = $options{b}\n" }
	if ($options{j} =~ m/^[N|n]$/) {
		print $fh "Skew-t shape parameter = 100 (hard coded)\n";
		print $fh "Skew-t df = 10 (hard coded)\n";
	}
	print $fh "----\n";	
}

if ($options{f} =~ m/^[6|7]$/) {
	
	print $fh "MCMCTREE options\n";
	# -----
	# constraints
	print $fh "Upper bound -u = $options{u}\n";
	
	# sequences
	if ($options{f}==7) {
		if ($options{S} == 1) { print $fh "Number of sequence replicates -S = $options{S} (default)\n" }
		else { print $fh "Number of sequence replicates -S = $options{S}\n" }
	}
	if ($options{L} == 1) { print $fh "Number of loci -L = $options{L} (default)\n" }
	else { print $fh "Number of loci -L = $options{L}\n" }
	
	# -----
	# clock model
	if ($options{z} == 0) { print $fh "Strict clock model implemented: variance -z = $options{z}\n" }
	elsif ($options{z} == 0.01) { print $fh "Independent rates model: variance -z = $options{z} (default)\n" }
	else { print $fh "Independent rates model: variance -z = $options{z}\n" }
	
	# -----
	# mcmc settings
	if ($options{f}==6) {
		if ($options{X} == 20000) { print $fh "Number of steps in the MC chain (prior) -X = $options{X} (default)\n" }
		else { print $fh "Number of steps in the MC chain (prior) -X = $options{X}\n" }
	}
	if ($options{f}==7) {
		if ($options{x} == 20000) { print $fh "Number of steps in the MC chain (posterior) -x = $options{x} (default)\n" }
		else { print $fh "Number of steps in the MC chain (posterior) -x = $options{x}\n" }
	}
	# -----
	# misc
	$mcmctree_catch = 1 if defined $options{R};
	if ($mcmctree_catch == 1) {
		print $fh "MCMCTREE-CATCH -R = specified.\n";
		print $fh "MCMCTREE-CATCH forces MCMCTREE to restart 5 times when it crashes before the end of a run.\n";
		print $fh "However, when this happens it strongly suggests there is a problem whith the data.\n";
	}

	# -----
	
	print $fh "----\n";

}

if ($options{f}==18) {
	print $fh "Number of loci -L = $options{L} (no default)\n";
}

# -------
print $fh "log ID (-l) = $options{l}\n";
print $fh "----\n";

# -------

if ($options{f} =~ m/^[2|3|4|8|6|7]$/) {
	print $fh "Seed used (-B) = $options{B}\n";
	print $fh "----\n";
}

# -------

close $fh;

# -------

my $logID=$options{l};

system("mv $filename $logID.$filename");

# EOF

}

1;