#!/usr/bin/perl
use strict;
use warnings;

# this function prints out the program options if -h is specified 

sub PrintSwitches {
	
print "Program.pl options

This is beta version 0.0.1.
Please report all bugs to Rachel.Warnock\@gmail.com

- a \#	The age of the root used for simulating trees. It is also the maximum 
	age of basin, required for simulating fossils and later for generating 
	constraints, so the program can work out the maximum age of each fossil 
	bearing horizon. Default = 5.

- A \#	The peak abundance (PA) parameter in the model used to simulate non-
	uniform occurrence data (Holland 1995). No default value.

- B \#  Random seed integer.  
	
- b #	Soft maximum bound tail value. The default in MCMCTREE is 0.025,
	however here we use a default hard (techically sharp) bound (10^-300)
	
- c #	The scale parameter (c) of the truncated Cauchy distribution applied
	to the minimum bounds in MCMCTREE. Default = 1.
	
- C #	The number of nucleotide characters to include in the alignment, used
	for simulating sequence data using EVOLVER. Default = 1000.
	
- d	Debugging mode, equivalent to verbose mode. Prints out lots of garbage.

- D #	The preferred depth (PD) parameter in the model used to simulate non
	uniform occurrence data (Holland 1995). No default value.
	
- e #	Extinction/death rate (mu) used for simulating trees using TreeSim
	(Stadler, 2009). Default = 0.1

- f #	Program options. No default.
	*** Options for simulating data:
	-f 1 : parse nexus treefile
	-f 2 : simulate uniform occurrence data
	-f 3 : simulate uniform non-occurrence data
	-f 4 : simulate sequences
	-f 8 : simulate trees
	
	*** Options for analysis of simulated data.
	-f 5 : generates confidence intervals. 
	-f 6 : prior molecular clock analysis
	-f 7 : posterior molecular clock analysis		
	The results are output to the file “Results.txt”.	
	
	*** Miscellaneous options:
	-f 17 : parse simulated trees file for MPI run	

- F # 	tree number to start at. Default = 1.	

- g #	Origination/birth data (lamda) used for simulating trees using TreeSim
	(Stadler 2009).	Default = 1.
	
- h	Print all program options to the screen.

- i	Input switch for specifying the tree file containing simulated trees.
	Default = “simulated_trees.nex”.

- j N|U	Calibration priors: uniform or non-uniform density for minimum and
	maximum bounds.
	-j N : non-uniform
	-j U : uniform
	Default = U.
	
- K B|L|M|A|C|H|S
	Method used to derive constraints based on occurrence data.
	Current options include:
	-k L : minimum bounds only
	-k M : stratigraphic bracketing (Marshall, 2008; equation 14)
	-k A : arbitrary max (define the max using -U)
	-k H : Hedman (2010)
	-k B : phylogenetic bracketing
	Default = B.
	
-l	Log file ID - prepends the output files Program_Options.txt and/or Results.txt.

-L # Number of sequence loci. Default = 1.

- m #	Soft maximum bound tail value. The default in MCMCTREE is 0.025, 
	however here we use a default value of 0.01.

- M # 0|4	Substitution model. Options: 0 (JC69), 4 (HKY85). This numbering
	scheme follows PAML. Default = 4.

- n #	The number of terminal taxa. No default.
	
- P	Program directory. The program works this out. Don’t change this except 
	for use with individual functions.
	
- p #	The location parameter (p) of the truncated Cauchy distribution 
	applied to the minimum bounds in MCMCTREE. Default = 0.1.
	
- r #	Resolution. This refers to the total number of time bins/horizons 
	during the time interval from t = 0 to t = a. No default.

- s #	Sampling intensity. This refers to the probability of collecting a
	fossil during a given time bin when sampling is uniform. No default.
	
- S #	The number of sequence replicates for each tree. Default = 1.
	
- t #	total number of trees to be included in the analysis. No default.

- T #	The depth tolerance (DT) parameter in the model used to simulate non-
	uniform occurrence data (Holland 1995). No default value.
	
- u #	Upper bound. Maximum constrain applied at the root of the tree if no 
	other information is available. No default.
	
- U # Percentage of the minimum bounds to be implemented as maxima, for using with -k = A.
	Default = 1.5 (e.g. 150\%).
	
- v	verbose mode, prints out loads of garbage.

- w #	Maximum water depth. Used to simulate the water depth profile when the 
	preservation model is non-uniform. Default = 2.

- x #	Number of steps in the MC chain (MCMCTREE) (posterior analysis only). 
	Default = 40000.
	
- X #	Number of steps in the MC chain (MCMCTREE) (prior analysis only). 
	Default = 40000.

- y	For each function that requires an input tree, the tree is specified 
	using -y. Used for automation, otherwise used only for debugging
	purposes. 
	
- z #	the variance of the lognormal distribution for independent rates 
	models. Default = 0.01.	
";
		
die "\n";
}

1;
