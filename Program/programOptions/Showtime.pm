#!/usr/bin/perl
use strict;
use warnings;
use Option1; # parse nexus
use Option2; # simulate uniform occurrences
use Option3; # simulate non-uniform occurrences
use Option4; # simulate sequences
use Option5; # generate confidence intervals
use Option6; # prior 
use Option7; # posterior
use Option8; # simulate trees (THIS DOESN'T WORK)
use Option17; # Parse for MPI run
use Option18; # generate a results file including the median

# this function calls the selected program option into action

sub Showtime {

my $i=$_[0];
my %options=%$i;

# parse exisiting nexus file/simulated trees  

if ($options{f}==1) {
	Option1(\%options);
}

# simulating data (uniform occurrence data)

if ($options{f}==2) {
	Option2(\%options);
}

# simulating data (non-uniform occurrence data)

if ($options{f}==3) {
	Option3(\%options);
}

# simulating sequence data

if ($options{f}==4) {
	Option4(\%options);
}

# generate calibrations/CIs

if ($options{f}==5) {
	Option5(\%options);
}

# prior analysis

if ($options{f}==6) {
	Option6(\%options);
}

# posterior analysis

if ($options{f}==7) {
	Option7(\%options);
}

# simulate trees

if ($options{f}==8) {
	Option8(\%options);
}

# data parsing for MPI run

if ($options{f}==17) {
	Option17(\%options);
}

# generate results file inc. the median

if ($options{f}==18) {
	Option18(\%options);
}

# ----

}

1;

