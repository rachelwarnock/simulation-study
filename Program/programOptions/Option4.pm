#!/usr/bin/perl
use strict;
use warnings;
use SimSeq;

# Option4 simulates sequences for existing trees in existing sub folders

sub Option4 {

my $i=$_[0];
my %options=%$i;

# ParseNexus(\%options);

my $trees=$options{t};
my $tree_counter=$options{F};
my $stop=($tree_counter+$trees)-1;

foreach my $tree ($tree_counter..$stop) {
	my $dir="tree$tree_counter";
	chdir $dir;
	$options{y}="tree$tree_counter.txt";
	SimSeq(\%options);
	$dir="..";
	chdir $dir;
	$tree_counter=$tree_counter+1;
}
	
}

1;