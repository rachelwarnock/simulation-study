#!/usr/bin/perl
use strict;
use warnings;
use SimWaterDepth;
use SimNonUnifOccurrences;

# Option3 simulates non-uniform occurrence data for existing trees (in existing sub folders)

sub Option3 {

my $i=$_[0];
my %options=%$i;

SimWaterDepth(\%options);

my $trees=$options{t};
my $tree_counter=$options{F};
my $stop=($tree_counter+$trees)-1; 

foreach my $tree ($tree_counter..$stop) {
	my $dir="tree$tree_counter";
	chdir $dir;
	$options{y}="tree$tree_counter.txt";
	SimNonUnifOccurrences(\%options);
	$dir="..";
	chdir $dir;
	$tree_counter=$tree_counter+1;
}

}

1;
