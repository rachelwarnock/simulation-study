#!/usr/bin/perl
use strict;
use warnings;
use mcmctreeCatch;

# this script runs posterior analysis in mcmctree based on the approximate likelihood approach
# bugfix - mcmctree-catch implemented (-R), if mcmctree exits because it calculated an implausible likelihood score this function will restart analysis in mcmctree 5 times before giving up

sub PosteriorMCMCTREE {
	
my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{y};
my $variance=$options{z};
my $upperbound=$options{u};
my $program_dir=$options{P};
my $model=$options{M};
my $steps=$options{x};
my $seqreps=$options{S};
my $loci=$options{L};
my $seqrep_counter=0;

my $mcmctree_catch=0;
$mcmctree_catch=1 if defined $options{R};

# verbosity & debugging options
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
# create a label for all output files
my ($current_tree_abbrev, $txt) = split(/\./, $treefile);
# ----
# mcmc settings
my $samples=20000;
my $sampfreq=$steps/$samples;
my $burnin=$steps*0.1;

# ----

my $clock;
my $beta_variance; # this is the beta parameter used to specify the variance of the lognormal distribution. See below for $beta_rgene

if ($variance == 0) {
	$clock=1;
	$beta_variance=10; # under the strict clock model this parameter will be printed to the ctl file, but will not take effect
}
else {
	$clock=2;
	$beta_variance=1/$variance; # this is for a diffuse gamma prior (alpha = 1); beta is calculated using m=alpha/beta
}

# ------ 
my $commands;
my $outfile;

# usedata = 3 

# generate random seed for mcmctree
my $mcmc_seed = int(rand(2**31));

A: $outfile="$current_tree_abbrev.usedata3";
open (MCMC_CTL, ">$current_tree_abbrev.mcmctree.ctl") || die "can't open file";

$commands = "seed = $mcmc_seed
seqfile = $current_tree_abbrev.$seqrep_counter.seq
treefile = mcmctree.$current_tree_abbrev.txt
outfile = $outfile

ndata = $loci
seqtype = 0
usedata = 3
clock = $clock
RootAge = <$upperbound

model = $model
alpha = 0.2500
ncatG = 5
cleandata = 0

BDparas = 1 1 0
rgene_gamma = 1 1 1
sigma2_gamma = 1 10 1

finetune = 1: .05  0.1  0.12  0.1 .3

print = 1
burnin = $burnin
sampfreq = $sampfreq
nsample = $samples
* details of all parameters are provided in /Folder";

print MCMC_CTL $commands;
close MCMC_CTL;
my $baseml_dir=$program_dir."Program/paml4.?";
my $mcmctree=$program_dir."Program/paml4.?/mcmctree";
system ("$mcmctree $current_tree_abbrev.mcmctree.ctl $baseml_dir"); # this is a flag I added to mcmctree.c
system ("mv out.BV in.BV");
unless ($verbosity==1) {
	system ("rm 2base.t lnf rates rst* tmp* rub $current_tree_abbrev.usedata3");
}

# ------ 
# usedata = 2

# my $seed = 584093927; this is for debugging purposes
# note that if you specify a seed number (not -1) mcmctree does not print out the
# SeedUsed file

my $counter=0;

$outfile="$current_tree_abbrev.usedata2";
B: open (MCMC_CTL, ">$current_tree_abbrev.mcmctree.ctl") || die "can't open file";

$commands = "seed = $mcmc_seed
seqfile = $current_tree_abbrev.$seqrep_counter.seq
treefile = mcmctree.$current_tree_abbrev.txt
outfile = $outfile

ndata = $loci
seqtype = 0
usedata = 2 *
clock = $clock
RootAge = <$upperbound

model = $model
alpha = 0.2500
ncatG = 5
cleandata = 0

BDparas = 1 1 0
rgene_gamma = 2 2 1
sigma2_gamma = 1 10 1

finetune = 1: .1 .1 .1 .1 .1 .1

print = 1
burnin = $burnin
sampfreq = $sampfreq
nsample = $samples
* details of all parameters are provided in /Folder";

print MCMC_CTL $commands;
close MCMC_CTL;

system ("$mcmctree $current_tree_abbrev.mcmctree.ctl"); # when mcmctree is in the bin

# ---- mcmcmtree catch

$counter=$counter+1;

my $mcmc_fail=0; # has analysis in mcmctree worked?

if ($mcmctree_catch == 1) { # call the function mcmctreeCatch

	$mcmc_fail=mcmctreeCatch(\%options);

	if ($mcmc_fail == 1) {
		if ($counter <= 5) {
			# $seed = -1; for debugging purposes
			goto B;
		}
		else {
			print "I've tried rerunning mcmctree 5 times but it keeps crashing - something must be very wrong!\nCheck your files!\n";
		}
	}
}

# ---- tidy up

#unless ($verbosity==1) {
	#system ("rm FigTree.tre SeedUsed in.BV");
#	system ("rm SeedUsed");
#}

# ---- for multiple sequence rates
if ($seqreps > 1) {
	
	system ("mv mcmc.txt mcmc.L$loci.$seqrep_counter.out");
	$seqrep_counter=$seqrep_counter+1;
	
	if ($seqrep_counter < $seqreps) {
		goto A;
	}
}
else {
	system ("mv mcmc.txt mcmc.L$loci.out");
	system ("cp $current_tree_abbrev.usedata2 $current_tree_abbrev.L$loci.usedata2");
}

# ---- EOF

}

1;


