#!/usr/bin/perl
use strict;
use warnings;
#use Statistics::Descriptive;

# This function calculates the 95% HPDs from the MCMCTREE (mcmc.out) output
# This is the way Tracer calculates the HPDs

# By default the program changes the name of the mcmc.out file to mcmc.pr.out for analysis 
# without sequence data - specified when calling the function

# The mcmc.out file is deleted (unless in verbose mode)
# The purpose of this is so that results files aren't confused during multiple rounds of simualtions & analyses

# Note that if mcmctree has failed for some reason (e.g. calculated an implausible likelihood score, given the input), 
# this function will not calculate or output the mean and HPDs. Instead, the hashes that contain the mcmc sample values 
# for each node won't be created (in the loop labeled "split the colunms by headers and mcmc values").

sub CalculateHPDs {

my $i;	
$i=$_[0];
my %options=%$i;
$i=$_[1];
my $prior=$i;
$i=$_[2];
my $inc_median=$i;
$i=$_[3];
my $seqrep=$i;
my $reps=1;
my $loci=$options{L};
unless($options{f}==18){
	$reps=$options{S};
}
	
# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
# open output
if ($prior == 1) {
	
	if ($verbosity == 1) {
		print "Interested in the prior output (mcmc.pr.out)\n";
	}
	# priors
	open (MCMC_OUT, "mcmc.pr.out") || die "can't open mcmc.pr.out!";
}
elsif ($prior == 0) {
	
	if ($verbosity == 1) {
		print "Interested in the posterior output (mcmc.out)\n";
	}
	# posteriors
	# for multiple sequence replicates
	if ($reps > 1) {
		open (MCMC_OUT, "mcmc.L$loci.$seqrep.out") || die "can't open mcmc.out!";
	}
	else {
		open (MCMC_OUT, "mcmc.L$loci.out") || die "can't open mcmc.out!";
	}
}
else {
	die "Something wrong with the specification of the mcmc files\n";
}

my @lines=<MCMC_OUT>;

# ----
# calculate the number of mcmc steps
my $line_count=-1; # to allow for the header line

foreach my $lines (@lines) {
	unless ($lines =~ /^#/ || $lines =~ /^1\s+/) {
	# skip lines with comments, and the first step
		$line_count=$line_count+1;
	}
}

# ----
# burnin phase not printed to output files in mcmctree

# ----
# split the colunms by headers and mcmc values

my @headers;
my @values;
my %variables;

# place the headers in an array
foreach my $lines (@lines) {
	if ($lines =~ /^Gen/) {
		@headers=split(/\s+/, $lines);
	}
}

my $total_headers=scalar(@headers);

foreach my $lines (@lines) {	
	# skip the comments, the first mcmc state (=0) and the line containing the headers
	unless ($lines =~ /^#/ || $lines =~ /^1\s+/ || $lines =~ /Gen/) {		
		# post burnin
		# split the values in each column on each line
		@values=split(/\s+/, $lines);			
		foreach my $values (0..$total_headers-1) {		
			my $current_headers=$headers[$values];
			# create a hash containing the headers and their respective values
			push @{$variables{$current_headers}}, $values[$values];
			#my $count=scalar@{$variables{$current_headers}}; # debugging code
		}
	}
}

close MCMC_OUT;

# ------
# calculate the mean
my %tmrca_mean;

foreach my $tmrca (keys %variables) {

	if ($tmrca =~ /t_n/) {
		my $age_total=0;
		foreach my $age (@{$variables{$tmrca}}) {
			$age_total=$age_total+$age;
		}
		my $mean=$age_total/($line_count);
		$tmrca_mean{$tmrca}=$mean;
	}
}

# ------
# calculate the median

my %tmrca_median;

if($inc_median==1) {

	foreach my $tmrca (keys %variables) {
	
		if ($tmrca =~ /t_n/) {
			my $median=median(@{$variables{$tmrca}});
			$tmrca_median{$tmrca}=$median;
		}
	}
}

# ------
# calculate the geometric mean

my %tmrca_geometric;
my $stat;

if($inc_median==1) {

	foreach my $tmrca (keys %variables) {
	
		if ($tmrca =~ /t_n/) {
			$stat = Statistics::Descriptive::Full->new();
  			$stat->add_data(@{$variables{$tmrca}}); 
  			my $geometric_mean = $stat->geometric_mean();
			#my $geometric_mean=geometric_mean(@{$variables{$tmrca}});
			$tmrca_geometric{$tmrca}=$geometric_mean;
		}
	}
}

# ------
# calculate the mode

my %tmrca_mode;

if($inc_median==1) {

	foreach my $tmrca (keys %variables) {
	
		if ($tmrca =~ /t_n/) {
			$stat = Statistics::Descriptive::Full->new();
  			$stat->add_data(@{$variables{$tmrca}}); 
  			my $mode = $stat->mode();
			$tmrca_mode{$tmrca}=$mode;
		}
	}
}

# ------
# CPDs & HPDs
# Alexei Drummond explains the difference here:
# https://groups.google.com/forum/?fromgroups=#!searchin/beast-users/hpd/beast-users/cTrnb5lguh8/Cco7gFrefUYJ
# see also http://forums.cacti.net/post-95140.html&highlight=
# ------
# define the desired level of confidence
my $confidence_limit=0.95;
# ----
# calculate the 95% HPDs
# this is how tracer calculates CrIs
my %tmrca_lower_HPD;
my %tmrca_upper_HPD;
my $lower_Nth;
my $upper_Nth;
my $lower;
my $upper;
my $min_so_far;
my $next_difference;

foreach my $tmrca (keys %variables) {
	if ($tmrca =~ /t_n/) {
		
		# sort the age estimates from smallest to largest
		@{$variables{$tmrca}}=sort{$a <=> $b}@{$variables{$tmrca}};	
		
		# start at the lowest end of the distribution
		$lower_Nth=0;
		
		# calculate the upper 95th percentile
		my $Nth = rounding(scalar(@{$variables{$tmrca}}*$confidence_limit));	
		$upper_Nth=$Nth-1;
		
		# get the ages for the lower and upper ends of the 95th percentile
		$lower=@{$variables{$tmrca}}[$lower_Nth];
		$upper=@{$variables{$tmrca}}[$upper_Nth];

		# calculate the width of the CrIs
		$min_so_far=$upper-$lower;
		
		# define the values so far
		$tmrca_lower_HPD{$tmrca}=$lower;
		$tmrca_upper_HPD{$tmrca}=$upper;
				
		# perform an exhaustive search for the shorest HPD interval
		
		# for the remainder of the distribution
		foreach my $i (0..($line_count-$Nth-1)) {		
		# if the upper limit of this loop is defined correctly it should
		# output the same values as tracer

			# shift the distribution up a notch
			$lower_Nth=$lower_Nth+1;
			$upper_Nth=$upper_Nth+1;				
			$lower=@{$variables{$tmrca}}[$lower_Nth];
			$upper=@{$variables{$tmrca}}[$upper_Nth];
				
			# caculate the new CrI width
			$next_difference=$upper-$lower;

			# if this width is shorter than the minimum width so far
			if ($next_difference < $min_so_far) {

				# update the lower and upper limits of the HPD
				$tmrca_lower_HPD{$tmrca}=$lower;
				$tmrca_upper_HPD{$tmrca}=$upper;
					
				# redefine the minimum width
				$min_so_far=$next_difference;				
			}
		}		
	}
}

# check the mean and HPDs are correct
if ($verbosity ==1) {
	foreach my $tmrca(keys %variables) {
		if ($tmrca =~ /t_n/) {
			print "$tmrca mean: ", sprintf("%.3f", $tmrca_mean{$tmrca}), "\n";
			print "$tmrca lower : ", sprintf("%.4f", $tmrca_lower_HPD{$tmrca}), "\n";
			print "$tmrca upper : ", sprintf("%.4f", $tmrca_upper_HPD{$tmrca}), "\n";
			if($inc_median==1) {
				print "$tmrca median : ", sprintf("%.3f", $tmrca_median{$tmrca}), "\n";
				print "$tmrca geometric mean : ", sprintf("%.3f", $tmrca_geometric{$tmrca}), "\n";
				print "$tmrca mode : ", sprintf("%.3f", $tmrca_mode{$tmrca}), "\n";
			}
		}
	}
}

# ----
return(\%tmrca_mean,\%tmrca_lower_HPD,\%tmrca_upper_HPD,\%tmrca_median);

}

# ----
sub rounding {
	return int($_[0] + .5 * ($_[0] <=> 0));
}

sub median {
    my @vals = sort {$a <=> $b} @_;
    my $len = @vals;
    if($len%2) { #odd? 
    	return $vals[int($len/2)];
    }
    else { #even
		return ($vals[int($len/2)-1] + $vals[int($len/2)])/2;
    }
}

1;

