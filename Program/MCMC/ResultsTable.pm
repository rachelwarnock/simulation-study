#!/usr/bin/perl
use strict;
use warnings;
use BranchingTimes;
use ReadCalibrations;
use MCMCTREEResults;
use Scalar::Util qw(looks_like_number);

# this function prints the true node ages, the calibrations and the MCMC output to a single (tab delimited) table - Restults.txt


sub ResultsTable {

my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{y};
my $seqreps;
$seqreps=1 unless defined $options{S};
$seqreps=$options{S} if defined $options{S};

my $inc_median=0;
if($options{f}==18) {
	$inc_median=1;
}

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};
# ------
my @temp;

# Part A - fetch the true node ages

@temp=BranchingTimes(\%options);
# note $temp[0] = parents
$i=$temp[1];
my %true_ages=%$i;
$i=$temp[2];
my %childs=%$i;
undef @temp;
# ------

# Part B - fetch the calibrations

@temp=ReadCalibrations(\%options);
$i=$temp[0];
my %minima=%$i;
$i=$temp[1];
my %maxima=%$i;
undef @temp;

# ------

# Part C - fetch the mcmctree HPDs - priors

my $prior;
my %prior_mean;
my %prior_lower;
my %prior_upper;

if (($options{f}==7) or ($options{f}==6) or ($options{f}==18)) {
	
	$prior=1;

	@temp=MCMCTREEResults(\%options,$prior,$inc_median);

	$i=$temp[0];
	%prior_mean=%$i;
	$i=$temp[1];
	%prior_lower=%$i;
	$i=$temp[2];
	%prior_upper=%$i;
	undef @temp;

}

# ------
# Part C - fetch the mcmctree HPDs - posteriors

my %posterior_mean;
my %posterior_lower;
my %posterior_upper;
my %posterior_median;

my $seqrep=0; # seqrep counter

A: if (($options{f}==7) or ($options{f}==18)) {
	
	$prior=0;
	
	@temp=MCMCTREEResults(\%options,$prior,$inc_median,$seqrep);

	$i=$temp[0];
	%posterior_mean=%$i;
	$i=$temp[1];
	%posterior_lower=%$i;
	$i=$temp[2];
	%posterior_upper=%$i;
	$i=$temp[3];
	%posterior_median=%$i;
	undef @temp;
	
}
$seqrep=$seqrep+1;
# ------
# print the table
# is this a sensible place to round the results?
foreach my $node (keys %childs) { # begin loop 1
	
	if (exists $true_ages{$node}) { # 2

		print EVERYTHING "$node\t", sprintf("%.4f", $true_ages{$node}), "\t";
	} # 2
	else { # this should never really be the case # 3
		print EVERYTHING "NA\t";
	} # 3
	if (exists $minima{$node}) { # 4
	
		print EVERYTHING sprintf("%.4f", $minima{$node}), "\t", sprintf("%.4f", $maxima{$node}), "\t";
	} # 4
	else { # this should never really be the case # 5
		print EVERYTHING "NA\tNA\t";
	} # 5
	
	if (exists $prior_lower{$node}) { # 6
	
		print EVERYTHING sprintf("%.4f", $prior_mean{$node}), "\t", sprintf("%.4f", $prior_lower{$node}), "\t", sprintf("%.4f", $prior_upper{$node}), "\t";
	} # 6
	else { # 7
		print EVERYTHING "NA\tNA\tNA\t";
	} # 7
	if (exists $posterior_lower{$node}) { # 8

		##if ($posterior_lower{$node} =~ /\d/) {#- alternative for assessing whether this is a number
		if (looks_like_number($posterior_lower{$node})) { # 9
			print EVERYTHING sprintf("%.4f", $posterior_mean{$node}), "\t", sprintf("%.4f", $posterior_lower{$node}), "\t", sprintf("%.4f", $posterior_upper{$node}), "\t";
		} # 9
		else { # 10
			print EVERYTHING "na", "\t", $posterior_lower{$node}, "\t", $posterior_upper{$node}, "\t";
		} # 10
		# median
		if($inc_median==1){
			print EVERYTHING sprintf("%.4f", $posterior_median{$node}),"\t";
		}
	} # 8
	else { # 11
		print EVERYTHING "NA\tNA\tNA\t";
	} # 11
	
	print EVERYTHING "\n";
	
} # 1

unless ($seqrep == $seqreps) {
	goto A;
}

# ----
# end of entire function

}

1;
