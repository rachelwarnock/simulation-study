#!/usr/bin/perl
use strict;
use warnings;

# this function reads the calibrations from the file "constraints.txt"
# the calibrations are neatly printed and stored in this file when the constraints are generated

sub ReadCalibrations {
	
my $i=$_[0];
my %options=%$i;
	
# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};
# ----
# read calibrations

open (CALS, "constraints.txt") || die "can't find the tree file";

my @lines=<CALS>;	

my %minima;
my %maxima;

foreach my $lines(@lines) {
	if ($lines =~ m/^t/ ) {
		my ($node, $bounds) = split(/:\s/, $lines);
		my ($min, $max) = split(/\s/, $bounds);
		chomp $min;
		chomp $max;
		$minima{$node}=$min;		
		$maxima{$node}=$max;		
	}
}

close CALS;

# examine the structure of the hash 'occurrences'
if ($verbosity == 1) {
	for my $node (keys %minima) {
		print "$node: min = $minima{$node} max = $maxima{$node}\n";
	}

}

return(\%minima,\%maxima);

}

1;