#!/usr/bin/perl
use strict;
use warnings;

# This function calculates the standard deviation and the naive standard error of the mean from the MCMCTREE (mcmc.out) output
# Note this is NOT the way Tracer calculates the SEMs

sub CalculateSEMs {
	
my $i;	
$i=$_[0];
my %options=%$i;
$i=$_[1];
my $prior=$i;
	
# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
# open output
if ($prior == 1) {
	
	if ($verbosity == 1) {
		print "Interested in the prior output (mcmc.pr.out)\n";
	}
	# priors
	open (MCMC_OUT, "mcmc.pr.out") || die "can't open mcmc.pr.out!";
}
elsif ($prior == 0) {
	
	if ($verbosity == 1) {
		print "Interested in the posterior output (mcmc.out)\n";
	}
	# posteriors
	open (MCMC_OUT, "mcmc.out") || die "can't open mcmc.out!";
}
else {
	die "Something wrong with the specification of the mcmc files\n";
}

my @lines=<MCMC_OUT>;

# ----
# calculate the number of mcmc steps
my $line_count=-1; # to allow for the header line

foreach my $lines (@lines) {
	unless ($lines =~ /^#/ || $lines =~ /^1\s+/) {
	# skip lines with comments, and the first step
		$line_count=$line_count+1;
	}
}

# ----
# burnin phase not printed to output files in mcmctree

# ----
# split the colunms by headers and mcmc values

my @headers;
my @values;
my %variables;

# place the headers in an array
foreach my $lines (@lines) {
	if ($lines =~ /^Gen/) {
		@headers=split(/\s+/, $lines);
	}
}

my $total_headers=scalar(@headers);

foreach my $lines (@lines) {	
	# skip the comments, the first mcmc state (=0) and the line containing the headers
	unless ($lines =~ /^#/ || $lines =~ /^1\s+/ || $lines =~ /Gen/) {		
		# post burnin
		# split the values in each column on each line
		@values=split(/\s+/, $lines);			
		foreach my $values (0..$total_headers-1) {		
			my $current_headers=$headers[$values];
			# create a hash containing the headers and their respective values
			push @{$variables{$current_headers}}, $values[$values];
			#my $count=scalar@{$variables{$current_headers}}; # debugging
		}
	}
}

close MCMC_OUT;

# ----
# calculate the mean
my %tmrca_mean;

foreach my $tmrca (keys %variables) {

	if ($tmrca =~ /t_n/) {
		my $age_total=0;
		foreach my $age (@{$variables{$tmrca}}) {
			$age_total=$age_total+$age;
		}
		my $mean=$age_total/($line_count);
		$tmrca_mean{$tmrca}=$mean;
	}
}

# ----
# calculate the standard deviation
my %tmrca_SDs;
my %tmrca_SEMs;
my $age_diff;
my $age_sq;
my $age_sq_total;
my $age_sq_mean;
my $age_SD;
my $SEM;
foreach my $tmrca (keys %variables) {
	
	if ($tmrca =~ /t_n/) {
		my $mean=$tmrca_mean{$tmrca};
		$age_sq_total=0;
		foreach my $age (@{$variables{$tmrca}}) {
			$age_diff=$age-$mean;
			$age_sq=$age_diff ** 2;
			$age_sq_total=$age_sq_total+$age_sq;
		}
		#print "$age_sq_total\n";
		# standard deviation
		$age_sq_mean=$age_sq_total/$line_count;
		$age_SD=$age_sq_mean ** (1/2);
		$tmrca_SDs{$tmrca}=$age_SD;
		
		# standard error of the mean
		$SEM=$age_SD/($line_count ** (1/2));
		$tmrca_SEMs{$tmrca}=$SEM;
		
	}
}
# ----
# Calculate the range of values based on the mean +/1 1SD
my %tmrca_SD_max;
my %tmrca_SD_min;
foreach my $tmrca(keys %variables) {
	if ($tmrca =~ /t_n/) {
		$tmrca_SD_max{$tmrca}=$tmrca_mean{$tmrca}+$tmrca_SDs{$tmrca};
		$tmrca_SD_min{$tmrca}=$tmrca_mean{$tmrca}-$tmrca_SDs{$tmrca};
	}
}

# ----

# check the mean and SEMs are correct
if ($verbosity ==1) {
	foreach my $tmrca(keys %variables) {
		if ($tmrca =~ /t_n/) {
			print "$tmrca mean: ", sprintf("%.3f", $tmrca_mean{$tmrca}), "\n";
			print "$tmrca SDs: ", sprintf("%.6f", $tmrca_SDs{$tmrca}), "\n";
			print "$tmrca SEM: ", sprintf("%.6f", $tmrca_SEMs{$tmrca}), "\n";
			# age range based on SDs
			print "$tmrca min: ", sprintf("%.3f", $tmrca_SD_min{$tmrca}), "\n";
			print "$tmrca max: ", sprintf("%.3f", $tmrca_SD_max{$tmrca}), "\n";			
		}
	}
}

# ----
return(\%tmrca_mean,\%tmrca_SD_min,\%tmrca_SD_max);

# ----
# end of function
}

1;