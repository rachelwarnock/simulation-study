#!/usr/bin/perl
use strict;
use warnings;

# this function works whether there are enough samples in the mcmc.out file. Currently the number of mcmc samples is hard coded (= 10000) 

sub mcmctreeCatch {

my $i=$_[0];
my %options=%$i;
my $seqrep=$_[1];
my $reps=$options{S};
my $loci=$options{L};

# verbosity & debugging options
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
my $samples=10000; # number of required 
my $wc_mcmc; # number of lines in mcmc.out

# use the system commmand "wc -l" to count the number of lines in the mcmc.out file
if ($reps == 1) {
	$wc_mcmc = `wc -l mcmc.L$loci.out`;
}
else {
	$wc_mcmc = `wc -l mcmc.L$loci.$seqrep.out`;
}

# parse the system message to get the line number
# it should looks something like "\t10002 mcmc1.out" or "\t0 mcmc1.out"
chomp $wc_mcmc;
if ($reps == 1) {
	$wc_mcmc =~ s/mcmc.L$loci.out//g;
}
else {
	$wc_mcmc =~ s/mcmc.L$loci.$seqrep.out//g;
}
$wc_mcmc =~ s/\s//g;

my $mcmc_fail=0; # this variable will tell the program if mcmctree has failed (=1) or run sucessfully (=0)

# if the number of lines is less than the required number of mcmc samples
if ($wc_mcmc <= $samples) {
	
	# record the failure
	$mcmc_fail=1;
		
	if ($verbosity == 1) {
		print "something went wrong!\n";
		print "Number of lines in mcmc.out = $wc_mcmc\n";
	}

}

return($mcmc_fail);

}

1;
