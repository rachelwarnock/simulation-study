#!/usr/bin/perl
use strict;
use warnings;

# this script runs analysis without sequence data in mcmctree

sub PriorMCMCTREE {
	
my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{y};
my $variance=$options{z};
my $upperbound=$options{u};
my $program_dir=$options{P};
my $model=$options{M};
my $steps=$options{X};
my $loci=$options{L};

# verbosity & debugging options
$options{v}=1 if defined $options{v};
$options{d}=1 if defined $options{d};

# ------
# create a label for all output files
my ($current_tree_abbrev, $txt) = split(/\./, $treefile);
# ----
# mcmc settings
my $samples=20000;
my $sampfreq=$steps/$samples;
my $burnin=$steps*0.1; 
# ----

my $clock;
my $beta_variance; # this is the beta parameter used to specify the variance of the lognormal distribution. See below for $beta_rgene

if ($variance == 0) {
	$clock=1;
	$beta_variance=10; # under the strict clock model this parameter will be printed to the ctl file, but will not take effect
}
else {
	$clock=2;
	$beta_variance=1/$variance; # this is for a diffuse gamma prior (alpha = 1); beta is calculated using m=alpha/beta
}

my $outfile_prior="$current_tree_abbrev.usedata0";

# generate random seed for mcmctree
my $mcmc_seed = int(rand(2**31));

# print mcmctree commands to the control file
open (MCMC_CTL, ">$current_tree_abbrev.mcmctree_pr.ctl") || die "can't open file";

my $commands_pr = "seed = $mcmc_seed
seqfile = $current_tree_abbrev.0.seq
treefile = mcmctree.$current_tree_abbrev.txt
outfile = $outfile_prior

ndata = $loci
seqtype = 0
usedata = 0
clock = $clock
RootAge = <$upperbound

model = $model
alpha = 0.2500
ncatG = 5
cleandata = 0

BDparas = 1 1 0
rgene_gamma = 2 2 1
sigma2_gamma = 1 10 1 * 1 $beta_variance 1

finetune = 1: .05  0.1  0.12  0.1 .3

print = 1
burnin = $burnin
sampfreq = $sampfreq
nsample = $samples
* details of all parameters are provided in /Folder";

print MCMC_CTL $commands_pr;
close MCMC_CTL;

my $mcmctree=$program_dir."Program/paml4.?/mcmctree";
system ("$mcmctree $current_tree_abbrev.mcmctree_pr.ctl");
system ("mv mcmc.txt mcmc.pr.out");
#system ("rm SeedUsed");

}

1;


