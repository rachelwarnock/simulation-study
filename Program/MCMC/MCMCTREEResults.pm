#!/usr/bin/perl
use strict;
use warnings;
use CalculateSEMs;
use CalculateHPDs;
use GrabMCMCTREEtree;
use ReadTree;
use ReadMCMCTREENodeLabels;
use mcmctreeCatch;

# This function matches the MCMCTREE and STFAS node labels, and the 95% HPDs
# The function is looks for prior (mcmc.pr.out) or posterior (mcmc.out) output

sub MCMCTREEResults {

my $i;	
$i=$_[0];
my %options=%$i;
$i=$_[1];
my $prior=$i;
$i=$_[2];
my $inc_median=$i;
$i=$_[3];
my $seqrep=$i;

# required input
my $treefile=$options{y};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
# create a label for all output files
my ($current_tree_abbrev, $txt) = split(/\./, $treefile);
# ----

my $mcmc_fail = 0;

if($options{f} != 18){
	if ($prior == 0) {
		$mcmc_fail = mcmctreeCatch(\%options,$seqrep);
	
		if ($verbosity == 1) {
			print "mcmc_fail = $mcmc_fail\n";
		}
	}
}

# ----
my @temp;
# ----
# calculate the range of ages within 1SD of the mean

# calculate SDs

my $calculate_SDs=0;

my %tmrca_SD_min;
my %tmrca_SD_max;

if ($calculate_SDs == 1) {
	if ($mcmc_fail == 0) {

		@temp=CalculateSEMs(\%options,$prior);
		$i=$temp[1];
		%tmrca_SD_min=%$i;
		$i=$temp[2];
		%tmrca_SD_max=%$i;
	}
}

undef @temp;

# ----
# calculate the HPDs;

my %tmrca_mean;
my %tmrca_lower_HPD;
my %tmrca_upper_HPD;
my %tmrca_median;

if ($mcmc_fail == 0) {

	@temp=CalculateHPDs(\%options,$prior,$inc_median,$seqrep);

	$i=$temp[0]; # first parameter is stored in $temp[0] 
	%tmrca_mean=%$i; # place %nodeup in hash %parent

	$i=$temp[1]; # second parameter is stored in $temp[1]
	%tmrca_lower_HPD=%$i; # place %distances in hash %distances

	$i=$temp[2]; # third parameter is stored in $temp[2]
	%tmrca_upper_HPD=%$i; # place %nodedown in hash %childs
	
	$i=$temp[3]; # third parameter is stored in $temp[2]
	%tmrca_median=%$i; # place %nodedown in hash %childs
	
}

undef @temp;
# ------
# fetch the tree with the node labels
my $mcmctree_tree;

if ($mcmc_fail == 0) {
	$mcmctree_tree = GrabMCMCTREEtree($current_tree_abbrev,$prior,$verbosity);
}
# ------

# ReadTree - subroutine written by Julian Gough
if ($mcmc_fail == 0) {
	@temp=ReadMCMCTREENodeLabels($mcmctree_tree,$verbosity); # subroutine returns (\%nodeup,\%distances,\%nodedown);
}
elsif ($mcmc_fail == 1) {
	@temp=ReadTree($treefile,$verbosity);
}

$i=$temp[1]; # second parameter is stored in $temp[1]
my %nodelabels=%$i; # place node labels in hash %nodelabels

$i=$temp[2]; # third parameter is stored in $temp[2]
my %childs=%$i; # place %nodedown in hash %childs
# ------

# assign the results to the correct nodes
# althought this set of hashes refers to the prior
# these also apply to posterior values 
my %prior_mean;
my %prior_upper;
my %prior_lower;
my %p_median;
my %SD_min;
my %SD_max;

foreach my $node (keys %childs) {
	
	if ($mcmc_fail == 0) {
		$nodelabels{$node}='t_n'.$nodelabels{$node};
		$prior_mean{$node}=$tmrca_mean{$nodelabels{$node}};
		$prior_lower{$node}=$tmrca_lower_HPD{$nodelabels{$node}};
		$prior_upper{$node}=$tmrca_upper_HPD{$nodelabels{$node}};
		if($inc_median==1) {			
			$p_median{$node}=$tmrca_median{$nodelabels{$node}};			
		}
		if ($calculate_SDs == 1) {
			$SD_min{$node}=$tmrca_SD_min{$nodelabels{$node}};
			$SD_max{$node}=$tmrca_SD_max{$nodelabels{$node}};
		}
		elsif ($calculate_SDs == 0) {
			$SD_min{$node}='--';	
			$SD_max{$node}='--';
		}		
	}
	elsif ($mcmc_fail == 1) {
		$prior_mean{$node}='na';
		$prior_upper{$node}='na';
		$prior_lower{$node}='na';
		if($inc_median==1) {			
			$p_median{$node}='na';
		}
		if ($calculate_SDs == 1) {
			$SD_min{$node}='na';	
			$SD_max{$node}='na';
		}
	}
}

# ------
if ($verbosity == 1) {
	foreach my $node (keys %childs) {
		print "Node ID : $node\n";
		print "Mean : $prior_mean{$node}\n";
		print "Lower HPD interval : $prior_lower{$node}\n";
		print "Upper HPD interval : $prior_upper{$node}\n";
		#print "SD min : $SD_min{$node}\n";
		#print "SD max : $SD_max{$node}\n";		
	}
	print "# -----\n";
}

return(\%prior_mean,\%prior_lower,\%prior_upper,\%p_median,\%SD_min,\%SD_max);

}

1;