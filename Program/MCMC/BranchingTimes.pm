#!/usr/bin/perl
use strict;
use warnings;
use ReadTree;

# This function calculates the true node ages from the specified treefile
# True node ages are returned in a hash

sub BranchingTimes {

my $i=$_[0];
my %options=%$i;

# required input
my $treefile=$options{y};

# verbosity
my $verbosity=0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
#ReadTree - subroutine written by Julian Gough
my @temp=ReadTree($treefile,$verbosity); # subroutine returns (\%nodeup,\%distances,\%nodedown);

$i=$temp[0]; # first parameter is stored in $temp[0] 
my %parent=%$i; # place %nodeup in hash %parent

$i=$temp[1]; # second parameter is stored in $temp[1]
my %distances=%$i; # place %distances in hash %distances

$i=$temp[2]; # third parameter is stored in $temp[2]
my %childs=%$i; # place %nodedown in hash %childs

# ----
# calculate the number of terminals & the number of nodes
# required to define the maximum number of times loop XX looks for a parent node
my $terminals=0;
foreach my $terminal (keys %parent) {
	unless ($terminal =~/:/) {
		$terminals=$terminals+1;
	}
}

if ($verbosity == 1) {
	print "Number of terminals: $terminals\n";
}

my $max_nodes=$terminals-2;

if ($verbosity == 1) {
	print "Maximum number of times to traverse the tree (= node total - 1): $max_nodes\n";
	print "# ----\n";	
}

# ----
my %true_ages;
my $age = 0;

foreach my $node (keys %childs) { # begin loop 1

	if ($verbosity == 1) {
		print "Node : $node\n";
	}
	
	if (exists $childs{$node}) { # begin loop 2
	
		if ($verbosity == 1) {
			print "Descendent nodes : $childs{$node}\n";
		}
		
		my ($first, $second) = split(/,/,$childs{$node});		
		
		my $child=$first;
		
		if ($verbosity == 1) {
			print "Let's go with the first : $first\n";
		}
		
		$age=$distances{$child};
		
		if ($verbosity == 1) {		
			print "Current age estimate = $age\n";
		}
		
		foreach my $i (1..$max_nodes) { # begin loop 3
	
			if (exists($childs{$child})) { # begin loop 4
			
				if ($verbosity == 1) {
					print "Next descendent : $childs{$child}\n";
				}	
								
				($first, $second) = split(/,/,$childs{$child});
			
				$child=$first;
				
				if ($verbosity == 1) {
					print "Let's go with the first : $first\n";
				}	
				
				$age=$age+$distances{$child};
				
				if ($verbosity == 1) {
					print "Current age estimate = $age\n";
				}				
				
			} # end loop 4			
		} # end loop 3

		$true_ages{$node}=$age;
		
		if ($verbosity == 1) {
			print "Final age estimate for $node = $age\n";
			print "# ----\n";
		}
		
	} # end loop 2
} # end loop 1

return(\%parent,\%true_ages,\%childs);
	
}

1; 