#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# MCMCTREEResults.pl
# test function: MCMCTREEResults
# package: MCMC
# other functions required: CalculateHPDs, GrabMCMCTREEtree, ReadMCMCTREENodeLabels, mcmctreeCatch

my %options=();

getopts("dy:v", \%options);

# function options
if (!$options{y}) {
	die "Specify the treefile using -y!\n";
}

# Are you intereted in the prior (= 0) or posterior output (= 1)?
my $prior=0;

# use $options{v} or $options{d} to switch on verbose mode

# ----

MCMCTREEResults(\%options,$prior);

# ----
# specify file paths

my $current_dir; # test script directory
my $mcmc; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$mcmc=$_;

}
	

BEGIN {

	use lib $mcmc;
	use MCMCTREEResults;

}
