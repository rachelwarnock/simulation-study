#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# ReadCalibrations.pl
# test function: ReadCalibrations
# package: MCMC
# other functions required: none

my %options=();

getopts("dv", \%options);

# use $options{v} or $options{d} to switch on verbose mode

# ----

ReadCalibrations(\%options);

# ----
# specify file paths

my $current_dir; # test script directory
my $mcmc; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$mcmc=$_;
	
}

BEGIN {
	
	use lib $mcmc;
	use ReadCalibrations;

}
