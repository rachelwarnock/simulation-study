#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# PriorMCMCTREE.pl
# test function: PriorMCMCTREE
# package: MCMC
# other functions required: none

my %options=();

getopts("dM:Ru:vx:y:z:", \%options);

# function options
if (!$options{y}) {
	die "Specify the treefile using -y!\n";
}
if (!$options{z}) {
	die "Specify the variance using -z!\n";
}
if (!$options{u}) {
	die "Specify the upper bound using -u!\n";
}
if (!$options{M}) {
	die "Specify the substitution model using -M! (Default = 4)\n";
}
if (!$options{x}) {
	die "Specify the chain length using -x! (Default = 100000)\n";
}

# use $options{v} or $options{d} to switch on verbose mode

# ----
my $program_dir; # Program directory
$options{P}=$program_dir;

# ----

PosteriorMCMCTREE(\%options);

# ----
# specify file paths

my $current_dir; # test script directory
my $mcmc; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$mcmc=$_;
}

BEGIN {

	use lib $mcmc;
	use PosteriorMCMCTREE;

}

BEGIN {
	
	# program directory
	$program_dir=$mcmc;
	$program_dir =~ s/Program\/MCMC\///g;
	$options{P}=$program_dir;
	
}
