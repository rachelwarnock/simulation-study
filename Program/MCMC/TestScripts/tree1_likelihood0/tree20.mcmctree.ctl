seed = 584093927
seqfile = tree20.0.seq
treefile = mcmctree.tree20.txt
outfile = tree20.usedata2

ndata = 1
seqtype = 0
usedata = 2
clock = 2
RootAge = <10

model = 4
alpha = 0.2500
ncatG = 5
cleandata = 0

BDparas = 1 1 0
rgene_gamma = 2 2
sigma2_gamma = 1 10 * 0.5 1

finetune = 1: .05  0.1  0.12  0.1 .3

print = 1
burnin = 10000
sampfreq = 10
nsample = 10000
* details of all parameters are provided in /Folder