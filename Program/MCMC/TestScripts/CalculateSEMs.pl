#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# CalculateSEMs.pl
# test function: CalculateSEMs
# package: MCMC
# other functions required: none, but use mcmctreeCatch to check analysis in mcmctree was successful

my %options=();

getopts("dv", \%options);

# function options

# Are you intereted in the prior (= 1) or posterior output (= 0)?
my $prior=0;

# use $options{v} or $options{d} to switch on verbose mode

# ----
# check mcmctree has worked

my $mcmc_fail=0;

if ($prior == 0) {
	$mcmc_fail = mcmctreeCatch(\%options);

	if ($mcmc_fail == 1) {
		print "Something went wrong mcmc_fail = $mcmc_fail!\n";
	}
}


# ----

if ($mcmc_fail == 0) {

	CalculateSEMs(\%options,$prior);
}

# ----
# specify file paths

my $current_dir; # test script directory
my $mcmc; # mcmc functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$mcmc=$_;
}

BEGIN {

	use lib $mcmc;
	use CalculateSEMs;
	use mcmctreeCatch;

}
