#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# ResultsTable.pl
# test function: ResultsTable
# package: MCMC
# other functions required: BranchingTimes, ReadCalibrations, MCMCTREEResults
# CalculateHPDs, GrabMCMCTREEtree, ReadMCMCTREENodeLabels, CalculateHPDsBEAST
# mcmctreeCatch
# other packages required: STFAS
# functions: ReadTree (required by BranchingTimes)

my %options=();

getopts("Bdf:y:v", \%options);

# function options
if (!$options{y}) {
	die "Specify the treefile using -y!\n";
}
if (!$options{f}) {
	die "Specify the program option using -f!\n";
}

# use $options{B} to analyze BEAST output

# use $options{v} or $options{d} to switch on verbose mode

# ----

open (EVERYTHING, ">Results.txt") || die "Can't open file 'Everything' \n";
open (EVERYMEAN, ">Results.inc.mean.txt") || die "Can't open file 'Everything' \n";
open (EVERYSD, ">Results.inc.SD.txt") || die "Can't open file 'Everything' \n";

ResultsTable(\%options);

close EVERYTHING;
close EVERYMEAN;
close EVERYSD;
# ----
# specify file paths

my $current_dir; # test script directory
my $stfas; # stfas functions library
my $mcmc; # mcmc functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$mcmc=$_;
	$stfas=$_;
	$stfas =~ s/MCMC/STFAS/g;
}

BEGIN {
	
	use lib $mcmc;
	use lib $stfas;
	use ResultsTable;

}
