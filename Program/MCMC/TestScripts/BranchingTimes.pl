#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# BranchingTimes.pl
# test function: BranchingTimes
# package: MCMC
# other packages required: STFAS
# functions required: ReadTree

my %options=();

getopts("dy:v", \%options);

# function options
if (!$options{y}) {
	die "Specify the treefile using -y!\n";
}

# use $options{v} or $options{d} to switch on verbose mode

# ----

BranchingTimes(\%options);

# ----
# specify file paths

my $current_dir; # test script directory
my $mcmc; # stfas functions library
my $stfas; # stfas functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$mcmc=$_;
	$stfas=$_;
	$stfas =~ s/MCMC/STFAS/g;

}
	

BEGIN {
	
	use lib $stfas;
	use lib $mcmc;
	use BranchingTimes;

}
