#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# ReadMCMCTREENodeLabels.pl
# test function: ReadMCMCTREENodeLabels
# package: MCMC
# other functions required: GrabMCMCTREEtree

my %options=();

getopts("dy:v", \%options);

# function options
if (!$options{y}) {
	die "Specify the treefile using -y!\n";
}

# use $options{v} or $options{d} to switch on verbose mode
my $verbosity = 0;
$verbosity=1 if defined $options{v};
$verbosity=1 if defined $options{d};

# ----
# create a label for all output files
my $treefile = $options{y};
my ($current_tree_abbrev, $txt) = split(/\./, $treefile);

# ----

my $tree_inc_node_labels=GrabMCMCTREEtree($current_tree_abbrev,$verbosity);

ReadMCMCTREENodeLabels($tree_inc_node_labels,$verbosity);

# ----
# specify file paths

my $current_dir; # test script directory
my $mcmc; # mcmc functions library

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$current_dir=$_;
	$mcmc=$_;

}
	

BEGIN {

	use lib $mcmc;
	use GrabMCMCTREEtree;
	use ReadMCMCTREENodeLabels;

}
