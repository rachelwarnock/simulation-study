#!/usr/bin/perl
use strict;
use warnings;

sub ReadMCMCTREENodeLabels { # v.2. reads sting containing the tree, rather than opening a treefile

# sub ReadTree
# This subroutine was written by Julian Gough & modified to read mcmctree node labels instead of distances
# ARGS: ReadTree($treefile)

my $treefile=$_[0]; # first argument passed to the subroutine: $treefile = -v $treefile
my $verbosity=$_[1]; # second argument passed to the subroutine: $verbosity = $ARGV[1]
my $leaf=''; # string $leaf begins empty. To contain the input tree, read from file $ARGV[0]
my @tree; # array containing all the components of the tree separated by a comma
my $i; # used to extract each component of the tree (separated by a ',') and place it in the @tree array
my $gen; # refers to relationships, used with $node
my $node; # refers to relationships, used with $gen
my %nodelabels; # hash contains nodelabels
my (%nodeup,%nodedown); # hashes contain details of relationship among nodes
my ($one,$two,$three); # contains regular expression components $one - node lables, $two - nodelabels, $three - not used
my $end; # contains concatenated regular expression components
my @middles; # unsure about the function of @middles, see loop 6 and 13 - 16
my $middle;
my $next=-1; # controls the counter
my $flag=1; # controls the counter

# my $treefile=$_[0]; # for use in subroutine mode

#open TREE,("$treefile");
#while ($treefile) {
#	if ($treefile =~ (/\S/)) { # if file contains non-whitespace character
#		$leaf=$leaf.$_; # add tree(s) to the string $leaf 
		# $_ Perl's default scalar, used when the control variable is omitted from the beginning a loop
		# "." string concatenation
#		chomp $leaf; # remove "\n" character at the end of the line
#	}
#}
#close TREE;
chomp $treefile;
$leaf=$treefile;
$leaf =~ s/\;//g; # replace ";" with an empty character

#my $root_label=$leaf;
my @parts=split / /,$leaf;
my $root_label=$parts[-2]; # the label for the root is the second last element of the array

@tree=split /,/,$leaf; # separate the tree into seperate components using the comma

foreach $leaf (@tree) {
	$leaf =~ s/\s//g; # remove spaces
	$leaf =~ s/\)/\):/g;
	$leaf =~ s/(\d+_\w+)/$1:0/g;
	$leaf =~ s/(\d+)_(\w+)/$2/g;
}

until ($flag == 0) { # begin loop 1
	
	$flag=0;
	$next=-1;
	
	foreach $i (0 .. scalar(@tree)-1) { # begin loop 2
	# forces the contents of () to be treated in a scalar context
	
		$leaf = $tree[$i]; # leaf is equal to each of the individual components of the tree
		
		unless ($leaf eq ':') {  # begin loop 3
		# unless $leaf has been reduced to ":"	
			
			unless ($next == -1) { # begin loop 4
			# unless counter is at -1
				
				if ($leaf =~ /^([\w:]+):(-?\d+\.?\d*[eE]?[-+]?\d*)\)(\S*)$/ ) { # begin loop 5
					# matches
					# 1. begins alphanumeric (inc. '_') or ':' one or more times
					# 2. ':'
					# 3. '-' 0 or once
					# 4. digit(s) once or more
					# 5. '.' 0 or once
					# 6. digit(s) any number of times (0 or more)
					# 7. ')'
					# 8. ends any set of characters other than white space (0 or more times)
					# e.g. matches 't3:0.3981461574):0.1645660237' or 't4:t1:0.4537474133)'
					# $1 = 't3'
					# $2 = '0.3981461574'
					# $3 = ':0.1645660237'

					$nodelabels{$1}=$2; # places branch lengths in the hash %nodelabels
					# for each taxon set ($1) branch lengths are equal to ($2)
					# e.g. for t3 => 0.3981461574
					
					$end="$1$3"; # e.g. t3:0.1645660237 (branch length to parent node, here t3 to root)
					
					$node=$gen; # current $gen specified in loop 16
					
					$one=$1; # contains node labels e.g. 't3' or 't4:t1'
					
					foreach $middle (@middles) { # loop 6
					# unclear what the purpose of this array is
						
						$middle =~ /^(\S+):(-?\d+\.?\d*),(\d+)$/;
						# should this include [eE]?[-+]?\d* ??
						# each $middle
						# matches
						# 1. begins any character other than whitespace once or more
						# 2. ':'
						# 3. '-' 0 or once
						# 4. digit(s) once or more
						# 5. '.' 0 or once
						# 6. digit(s) any number of times (0 or more)
						# 7. ','
						# 8. ends digit(s) once or more
						# e.g. ??
						# $1= ??
						# $3= ??
						
						$node=$node.':'.$1;
						
						$tree[$3]=':';
						
					} # end loop 6
					
					# loop 5 continued
					$tree[$i]=':';
					# reduce current component of the tree to ':'
					# e.g 't3:0.3981461574):0.1645660237' now equals ':'
					
					$node="$node:$end"; # e.g. node is now equal to '(t2:t3:0.1645660237'
					# includes distance of sister taxa to parent node
					
					$tree[$next]=$node; # place node in @tree array under string ID $next
					# e.g. '(t2:t3:0.1645660237'
					# I don't understand how this works - relates to the counter
					
					$node =~ s/:-?\d+\.?\d*[eE]?[-+]?\d*\)//g;
					# replace (branch lengths) 
					# 1. ':'
					# 2. '-' 0 or once
					# 3. digits (1 or more)
					# 4. '.' 0 or once
					# 5. digits (0 or more)
					# 6. ')'
					# with empty characters
					
					$node =~ s/:-?\d+\.?\d*[eE]?[-+]?\d*$//g;
					# replace (branch lengths) 
					# 1. ':'
					# 2. '-' 0 or once
					# 3. digits (1 or more)
					# 4. '.' 0 or once
					# 5. digits (0 or more)
					# 6. found at the end of the string
					# with empty characters
					
					$node =~ s/\)//g; # Replace ')' with white space from $node
					
					$node =~ s/\(//g; # Replace '(' with white space from $node
					
					$gen =~ s/\)//g; # Replace ')' with white space from $gen
					
					$gen =~ s/\(//g; # Replace '(' with white space from $gen
					
					$one =~ s/\)//g; # Replace ')' with white space from $one
					
					$one =~ s/\(//g; # Replace '(' with white space from $one
					
					$nodeup{$gen}=$node; # specifies relationships between "daughter" nodes and "parent" nodes in the hash %nodeup
					# e.g for $gen = t2 => t2:t3
					
					if (exists($nodedown{$node})) { # begin loop 7
						
						unless ($nodedown{$node}=~/,/) { # begin loop 8
						# unless hash element contains a comma
							
							$nodedown{$node}=$nodedown{$node}.",$gen"; # concatenate "parents" and "daughters"
							
						} # end loop 8
						
					} # end loop 7
					
					else { # begin loop 9
											
						$nodedown{$node}=$gen; # the nodedown from current $node is equal to $gen (e.g. t2 for $node = t2:t3)
						
					} # end loop 9
					
					$nodeup{$one}=$node; # the nodeup from current $one is equal to $node (e.g t2:t3 for $one = t3)
					
					if (exists($nodedown{$node})) { # begin loop 10
						
						unless ($nodedown{$node}=~/,/) { # begin loop 11
						# unless the element of the hash contains a comma
							
							$nodedown{$node}=$nodedown{$node}.",$one"; # (e.g. nodedown from the current $node = t2:3 is t2,t3
							
						} # end loop 11
						
					} # end loop 10
					
					else { # begin loop 12
						
						$nodedown{$node}=$one;
						
					} # end loop 12
					
					foreach $middle (@middles) { # begin loop 13 
					# loops 13 to 15 don't appear to do anything, perhaps necessary for larger trees
						
						$middle =~ /^([\w\:]+):(-?\d+\.?\d*),(\d+)$/;
						# matches
						# 1. begins alphanumber or ':' one or more times
						# 2. ':'
						# 3. '-' 0 or once
						# 4. digit(s) once or more
						# 5. '.' 0 or once
						# 6. digit(s) any number of times (0 or more)
						# 7. ','
						# 8. ends digit(s) once or more
						
						$one=$1;
						
						$two=$2;
						
						$nodelabels{$one}=$two;
						
						if (exists($nodedown{$node})) { # begin loop 14
							
							unless ($nodedown{$node}=~/,/) { # begin loop 15
								
								$nodedown{$node}=$nodedown{$node}.",$one";
								
							} # end loop 15
							
						} # end loop 14
						
						else { # begin loop 16
							
							$nodedown{$node}=$one;
							
						} # end loop 16
						
						$nodeup{$one}=$node;
						
					} # end loop 13
					
					$flag=1;
					
					$next=-1;
					
				} # end loop 5
				
				elsif ($leaf =~ /\)/ or $leaf =~ /\(/) { # begin loop 17
					
					$next=-1;
					
				} # end loop 
				
				else { # begin loop 18
					
					push @middles,"$leaf,$i";
					
				} # end loop 18
				
			}  # end loop 4
			
			if($leaf =~ /^(\S*)\(([\w:]+):(-?\d+\.?\d*[eE]?[-+]?\d*)$/) { # begin loop 19
				# matches
				# 1. begin any set of characters other than white space (0 or more times)
				# 2. '('
				# 3. # 1. begins alphanumber or ':' one or more times
				# 4. ':'
				# 5. '-' 0 or once
				# 4. digit(s) once or more
				# 5. '.' 0 or once
				# 6. ends digit(s) any number of times (0 or more)
				# e.g. '((t2:0.3981461574'
				# $1='('
				# $2='t2'
				# $3='0.3981461574'
				
				$nodelabels{$2}=$3; # place branch lengths in hash %nodelabels
				
				@middles=(); # empty array @middles
				
				$next=$i; # relates to the counter, not sure how this works
				
				$gen="$1$2"; # specifies generation
				
			} # end loop 19
			
		} # end loop 3
		
	} # end loop 2
	
} # end loop 1

$nodelabels{$node}=$root_label;
#$nodelabels{$node}=0;

# the following will illustrate how the tree is structured
if ($verbosity ==1) {
	
	print "%nodeup \n";
	while ((my $nodeup_key, my $nodeup_value) = each(%nodeup)){
		print $nodeup_key." -> ".$nodeup_value."\n";
	}

	print "\n %nodelabels \n";
	while ((my $distance_key, my $distance_value) = each(%nodelabels)){
		print $distance_key." -> ".$distance_value."\n";
	}

	print "\n %nodedown \n";
	while ((my $nodedown_key, my $nodedown_value) = each(%nodedown)){
		print $nodedown_key." -> ".$nodedown_value."\n";
	}
	print "# -----\n";
	
}

return(\%nodeup,\%nodelabels,\%nodedown); # backslash creates a reference to the hash
# refers to parents, nodelabels & childs

# end of subroutine ReadTree
}

1;