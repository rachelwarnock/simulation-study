#!/usr/bin/perl
use strict;
use warnings;

# GrabMCMCTREEtree takes the tree with node label IDs from the usedata output file

sub GrabMCMCTREEtree {

my $current_tree_abbrev=$_[0];
my $prior=$_[1];
my $verbosity=$_[2];

if ($prior == 1) {
	open (USEDATA, "$current_tree_abbrev.usedata0") || die "can't open the usedata file";
}
else {
	open (USEDATA, "$current_tree_abbrev.usedata2") || die "can't find the usedata file";
}

my @lines=<USEDATA>;
my $line_number=0; # create a line number counter
my $node_label_tree; # creat a sting for tree containing nodelabels

foreach my $lines (@lines) {
	$line_number=$line_number+1; # count the lines
	
	if ($lines =~ /Species/) {
	# if the line contains the word 'Species'
		
		$node_label_tree="$lines[$line_number]\n";
		# print out the next line contains the tree feat. node labels
	}
}
close USEDATA;

if ($verbosity == 1) {
	print "Node label IDs: $node_label_tree";
}

return($node_label_tree); # sub returns the tree feat. node labels
	
}

1;