#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Std;
use Cwd 'abs_path';

# define program libraries

my $program_dir; # program directory
my $lib; # program options library
my $stfas; # simulation functions library
my $mcmc; # mcmc functions library

# Find the current working directory

BEGIN {
	
	# find out the wkdir of the program
	$_=abs_path("$0");
	my @dirs=split(/\//,$_);
	shift(@dirs);
	pop(@dirs);
	$_='/';
	foreach my $dir (@dirs) {
		$_=$_."$dir/";
	}
	
	$program_dir=$_;
	$lib=$_."Program/programOptions";
	$stfas=$_."Program/STFAS";
	$mcmc=$_."Program/MCMC";
	
}

BEGIN {

	use lib $lib;
	use lib $stfas;
	use lib $mcmc;
	use ProgramOptions;
	use Showtime;
	
}

# -----
# read the input parameters and assign defaults

my %options=();
my $program_option;

getopts("A:a:B:b:c:C:dD:e:E:f:F:g:hi:j:k:l:L:m:M:n:N:o:p:q:r:Rs:S:t:T:u:U:vw:W:X:x:z:", \%options);

my $seed;

if($options{B}) {
	$seed = srand($options{B});
}
else{	
	$seed = srand();
	$options{B} = $seed;
}
	 
# sort out all the program options
my $i = ProgramOptions(\%options);
%options=%$i;

$options{P}=$program_dir;

# -----

Showtime(\%options);

# -----

END {	
	if($options{l}) {
		my $logID=$options{l};
		open(my $fh, ">>", "$logID.Program_Options.txt") or die "can't open > Program_Options.txt: $!";
	 	my $elapsed_seconds = time - $^T;
		print $fh qq(The process took ${elapsed_seconds}s to run\n); 
	}	
}

