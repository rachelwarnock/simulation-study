
cd output/phylo_bracketing
#cd output/strat_bracketing
#cd output/arbitrary_max

# mcmc analysis

reps=2

# posterior L = 2

seed=1265

for i in tree_*
do
  cd $i
  for j in `seq 1 $reps`
  do
  	perl ../../../Program.pl -f 7 -l f7L2.$j -t 1 -u 2 -F $j -B $seed -L 2 -x 320000
  	seed=$(($seed + 1))
  done	
  cd ../
done
