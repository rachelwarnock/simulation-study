
cd output

# generate constraints

reps=2

for i in fossils/tree_*
do
  dir="$i"
  dir+="_arb1"
  dir=$(echo $dir | cut -f 2 -d '/')
  cp -r $i $dir
  cd $dir
  perl ../../Program.pl -f 5 -l f5 -t $reps -r 50 -u 2 -k A -j N -U 1.1
  cd ../
done

for i in fossils/tree_*
do
  dir="$i"
  dir+="_arb2"
  dir=$(echo $dir | cut -f 2 -d '/')
  cp -r $i $dir
  cd $dir
  perl ../../Program.pl -f 5 -l f5 -t $reps -r 50 -u 2 -k A -j N -U 1.25
  cd ../
done

for i in fossils/tree_*
do
  dir="$i"
  dir+="_arb3"
  dir=$(echo $dir | cut -f 2 -d '/')
  cp -r $i $dir
  cd $dir
  perl ../../Program.pl -f 5 -l f5 -t $reps -r 50 -u 2 -k A -j N -U 1.5
  cd ../
done

for i in fossils/tree_*
do
  dir="$i"
  dir+="_arb4"
  dir=$(echo $dir | cut -f 2 -d '/')
  cp -r $i $dir
  cd $dir
  perl ../../Program.pl -f 5 -l f5 -t $reps -r 50 -u 2 -k A -j N -U 1.75
  cd ../
done

mkdir arbitrary_max
mv *_arb* arbitrary_max

